﻿using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RavadGarWebSite.Models.ViewModels;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;

namespace RavadGarWebSite.Controllers
{
    //[HandleError(ExceptionType = typeof(Exception), View = "~/Error/Index")]
    public class homeController : Controller
    {
        // GET: home
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetUserData()
        {
            var user = Session[SessionKeys.user] as Models.User;
            UserDataViewModel userData = null;
            if (user != null)
            {
                userData = new UserDataViewModel();
                userData.Emial = user.Email;
                userData.FName = user.FName;
                userData.LName = user.LName;
            }
            return View(userData);
        }
        //***********************************
        [HttpPost]
        public string searchFilter(string keyword)
        {
            var res = ProductUtility.GetProductByKeyword(keyword);
            var res2 = res.GetRange(0, 10 < res.Count ? 10 : res.Count).Select(s => new { s.PFName, s.PCode, s.Image });
            return JsonConvert.SerializeObject(res2);

        }
        [AllowAnonymous]
        public ActionResult signup()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        
        public ActionResult Register(RegisterViewModel u)
        {
            if (this.ModelState.IsValid)
            {
                var user = MakeUser(u);
                UserUtility.AddUser(user);
                TempData["RegisterResult"] = "The registeration is Succesfful";
            }
            else
                TempData["RegisterResult"] = "Error";
            return Redirect("/home/alerts");
        }
        [HttpPost]
        public string ExistEmail(string email)
        {
            var res=UserUtility.IsRegisteredEmail(email);
            ResponseClass response = new ResponseClass() { result = res.ToString() };
            return JsonConvert.SerializeObject(response);

        }
        public ActionResult alerts()
        {
            return View();
        }





        #region Utilities
        private User MakeUser(RegisterViewModel u)
        {
            var user = new User();
            user.Email = u.Email;
            user.FName = u.FName;
            user.LName = u.LName;
            user.Mobile = u.Mobile;
            user.Tel = u.Tel;
            user.PassWord = AccountingUtility.GetSecurePassword(u.Password);
            user.JoinDateTime = DateTime.Now;
            user.LastVisit = DateTime.Now;
            return user;
        }
        #endregion

    }
}