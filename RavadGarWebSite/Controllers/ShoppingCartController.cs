﻿using Newtonsoft.Json;
using RavadGarWebSite.Framwork.Attributes;
using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;

namespace RavadGarWebSite.Controllers
{
    public class ShoppingCartController : Controller
    {
        // GET: ShoppingCart
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult address()
        {
            var user = Session[SessionKeys.user] as Models.User;
            if (user != null)
            {
                var address = AddressUtility.GetAddressByUser(user.Email);
                var addressViewModel = AddressViewModel.ConvertToAddressViewModel(address);
                ViewBag.FatorId = Session["FactorId"].ToString();
                return View(addressViewModel);
            }
            return View();
        }
        public ActionResult FactorPeymant(string factorId)
        {
            var user = Session[SessionKeys.user] as Models.User;
            if (user != null)
            {
                var order = OrderUtility.GetOrderById(factorId);
                FactorViewModel factor = FactorViewModel.MakeFactor(order);
                return View(factor);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }


        [HttpPost]
        public string Order(List<OrderViewModel> products)
        {
            var user = Session[SessionKeys.user] as Models.User;
            if (user != null)
            {
                ResponseClass response = new ResponseClass();
                var factor = FactorViewModel.MakeOrder(products);
                factor.UserEmail = user.Email;
                //باید بررسی بشه که از این محصولات به تعداد کافی هست یا نه
                List<string> errors = new List<string>();

                foreach (var prod in products)
                {

                    if (prod.CombinationId != null)
                    {
                        int prodCombNumber = ProductUtility.GetProductCombinationCount(prod.CombinationId);
                        if (prodCombNumber < prod.Number)
                            errors.Add($"برای مجصول {prod.ProductName}- {prod.CombinationName} = {prod.CombinationValue} تعداد درخواستی {prod.Number} در انبار موجود نمی باشد.");
                    }
                    else
                    {
                        int productNumber = ProductUtility.GetProductNumber(prod.ProductId);
                        if (productNumber < prod.Number)
                            errors.Add($"برای مجصول {prod.ProductName}-  تعداد درخواستی {prod.Number} در انبار موجود نمی باشد.");
                    }
                }
                if (errors.Count == 0)
                {
                    OrderUtility.AddOrder(factor);
                    Session.Add("FactorId", factor.FactorId);


                    response.result = factor.FactorId;
                }
                else
                    response.result = string.Join("\r\n", errors);

                return JsonConvert.SerializeObject(response);
            }
            else
                return JsonConvert.SerializeObject(new ResponseClass() { result = "NoLogin" });
        }

        public ActionResult BankComback()
        {
            bool isError = false;
            string succeedMsg = string.Empty;
            string errorMsg = string.Empty;
            if (SamanPardakhtUtility.SamanPardakhtUtility.RequestUnpack(Request))
            {
                if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("OK"))
                {

                    ServicePointManager.ServerCertificateValidationCallback =
                               delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                    ///WebService Instance
                    var srv = new SamanPardakht.PaymentIFBindingSoapClient();
                    var result = srv.verifyTransaction(Request.Form["RefNum"], Request.Form["MID"]);

                    if (result > 0)
                    {
                        isError = false;
                        succeedMsg = "بانک صحت رسيد ديجيتالي شما را تصديق نمود. فرايند خريد تکميل گشت";

                    }

                    else
                    {
                        SamanPardakhtUtility.SamanPardakhtUtility.TransactionChecking((int)result);
                    }
                }
                else
                {
                    isError = true;
                    errorMsg = "متاسفانه بانک خريد شما را تاييد نکرده است";

                    if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Canceled By User") || SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals(string.Empty))
                    {
                        // Transaction was canceled by user
                        isError = true;
                        errorMsg = "تراكنش توسط خريدار كنسل شد";
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Invalid Amount"))
                    {
                        // Amount of revers teransaction is more than teransaction
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Invalid Transaction"))
                    {
                        // Can not find teransaction
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Invalid Card Number"))
                    {
                        // Card number is wrong
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("No Such Issuer"))
                    {
                        // Issuer can not find
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Expired Card Pick Up"))
                    {
                        // The card is expired
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Allowable PIN Tries Exceeded Pick Up"))
                    {
                        // For third time user enter a wrong PIN so card become invalid
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Incorrect PIN"))
                    {
                        // Pin card is wrong
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Exceeds Withdrawal Amount Limit"))
                    {
                        // Exceeds withdrawal from amount limit
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Transaction Cannot Be Completed"))
                    {
                        // PIN and PAD are currect but Transaction Cannot Be Completed
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Response Received Too Late"))
                    {
                        // Timeout occur
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Suspected Fraud Pick Up"))
                    {
                        // User did not insert cvv2 & expiredate or they are wrong.
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("No Sufficient Funds"))
                    {
                        // there are not suficient funds in the account
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("Issuer Down Slm"))
                    {
                        // The bank server is down
                    }
                    else if (SamanPardakhtUtility.SamanPardakhtUtility.transactionState.Equals("TME Error"))
                    {
                        // unknown error occur
                    }

                }

            }

            return View();

        }

        public ActionResult BackUrl()
        {
           
            string authority = Request.QueryString["au"];
            string status = Request.QueryString["rs"];
            string pin = "aBYK55QXjwruje1AG743";
            byte resultStatus = 1;
            long invoiceNumber = 1;
            long authorityNum = Int32.Parse(authority);
            if (status == "0" && authority != "-1")
            {
                ir.shaparak.pec.EShopService obj = new ir.shaparak.pec.EShopService();
                obj.PaymentEnquiry(pin, authorityNum, ref resultStatus, ref invoiceNumber);
                if (resultStatus == 0)
                {
                    var order = OrderUtility.GetOrderByAuthority(authority);
                    order.TransactionRefNumber = invoiceNumber.ToString();
                    order.PaymentState = "Payed";
                    EditOrderDetail(order);
                    Response.Redirect("/user/orders");
                    //send sms
                    SmsWebservice.Service smsObj = new SmsWebservice.Service();
                    string matn = "خرید از وب سایت \n";
                    foreach (var item in order.OrderDetails)
                    {
                        matn += item.Product.PFName + " قیمت" + item.Price + " تعداد " + item.Number+"\n";
                    }
                    matn += "مبلغ کل:" + order.Amount;
                    try
                    {
                        smsObj.SendSMS("nsms1495", "36398599", matn, "09129366991", "50002215602");


                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        smsObj.SendSMS("nsms1495", "36398599", matn, "09127017703", "50002215602");

                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        smsObj.SendSMS("nsms1495", "36398599", matn, "09207822670", "50002215602");

                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        smsObj.SendSMS("nsms1495", "36398599", matn, "09991499699", "50002215602");

                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        smsObj.SendSMS("nsms1495", "36398599", matn, "09372662168", "50002215602");

                    }
                    catch (Exception)
                    {
                    }
                }

            }
           SmsWebservice.Service smsObj2 = new SmsWebservice.Service();
            Response.Redirect("/ShoppingCart/ErrorPage/?status="+ status+"&resultStatus="+resultStatus);
            return View();
        }

        private void SmsObj_SendSMSCompleted(object sender, SmsWebservice.SendSMSCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public ActionResult ErrorPage()
        {
            return View();
        }
        private static void EditOrderDetail(Order order)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var currentOrder = entity.Orders.FirstOrDefault(f => f.FactorId == order.FactorId);
                //var currentOrder = entity.Orders.Attach(order);
                currentOrder.Amount = order.Amount;
                currentOrder.authority = order.authority;
                currentOrder.SubmitDateTime = order.SubmitDateTime;
                currentOrder.TransactionRefNumber = order.TransactionRefNumber;
                currentOrder.PaymentState = order.PaymentState;
                var res=entity.SaveChanges();
            }
        }
        public ActionResult GoToBank(string factorId)
        {
            var user = Session[SessionKeys.user] as Models.User;
            if (user != null)
            {
                var order = OrderUtility.GetOrderById(factorId);
                int amount = (int)order.Amount * 10;
                ir.shaparak.pec.EShopService obj = new ir.shaparak.pec.EShopService();
                string pin = "aBYK55QXjwruje1AG743";
                int orderid = Int32.Parse(order.FactorId);
                string callbackUrl = "http://ravadgar.com/ShoppingCart/BackUrl";
                long authority = 1;
                byte status = 0;
                order.authority = authority.ToString();
                obj.PinPaymentRequest(pin, amount, orderid, callbackUrl, ref authority, ref status);                if (authority == -1 || status != 0)
                {
                    return Content("status =" + status + "authority = " + authority);
                }
                else
                {
                    order.authority = authority.ToString();
                    EditOrderDetail(order);
                    Response.Redirect("https://pec.shaparak.ir/pecpaymentgateway/?au=" + authority);
                    return Content("redirect");
                }

            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }
    }
}