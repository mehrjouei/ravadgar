﻿using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;


namespace RavadGarWebSite.Controllers
{
    [HandleError(ExceptionType = typeof(Exception), View = "~/Error/Index")]
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index(int id = -1, string catTitle = "", string keyword = "")
        {
            List<Product> res = null;
            if (id != -1)
            {
                res = ProductUtility.GetProductsByCategoryId(id);
                ViewBag.id = id;
            }
            //else  if (catTitle!="")
            //{
            //    res = ProductUtility.GetProductByCategoryTitle(catTitle);
            //}
            else if (keyword != "")
            {
                res = ProductUtility.GetProductByKeyword(keyword);
                ViewBag.keyword = keyword;
            }
            return View(res);
        }

    }
}