﻿using Newtonsoft.Json;
using RavadGarWebSite.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RavadGarWebSite.Controllers
{
    [HandleError(ExceptionType =typeof(Exception),View ="~/Error/Index")]
    public class ProductController : Controller
    {
        // GET: Product
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult ProductList()
        {
            return View();
        }
        public ActionResult Index(int id,string keyword = "")
        {
            var product=ProductUtility.GetProduct(id);
            return View(product);
        }
        public ActionResult frameView(int id)
        {
            var product = ProductUtility.GetProduct(id);
            return View(product);
        }

        //**********************
        public string GetMostVisited()
        {
            var res=ProductUtility.GetMostVisitedProducts();
            return JsonConvert.SerializeObject(res);
        }
        public ActionResult GetTopNewProducts(int top)
        {
            var res = ProductUtility.GetTopNewProducts(top);
            return PartialView(res);
            //return JsonConvert.SerializeObject(res);
        }
        public ActionResult GetTopSellerProducts(int top)
        {
            var res = ProductUtility.GetTopsellerProducts(top);
            return PartialView(res);
            //return JsonConvert.SerializeObject(res);
        }

        public ActionResult GetMostSelled(int top)
        {
            var res = ProductUtility.GetTopMostSelled(top);
            return PartialView(res);
        }
        public ActionResult SearchInRockets()
        {
            return PartialView();
        }

    }
}