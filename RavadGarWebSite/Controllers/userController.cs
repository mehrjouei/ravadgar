﻿using CaptchaMvc.Attributes;
using Newtonsoft.Json;
using RavadGarWebSite.Framework.BaseClasses.Enum;
using RavadGarWebSite.Framwork.Attributes;
using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RavadGarWebSite.Controllers
{
    [MyAuthorize]
    public class userController : Controller
    {
        public User CurrentUser { get { return Session[SessionKeys.user] as User; } }
        // GET: user
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Address()
        {
            var address = AddressUtility.GetAddressByUser(CurrentUser.Email);
            var addressViewModel = AddressViewModel.ConvertToAddressViewModel(address);
            return View(addressViewModel);
        }

        [HttpPost]
        public string AddressEdit(AddressViewModel addressViewModel)
        {
            var response = new ResponseClass();
            if (ModelState.IsValid)
            {
                var address = addressViewModel.ConvertToAddress();
                address.Customer = (Session[SessionKeys.user] as Models.User).Email;
                AddressUtility.UpdateAddress(address);
                response.result = Enum.GetName(typeof(DBTransactionResult), DBTransactionResult.Successful);
            }
            else
                response.result = Enum.GetName(typeof(DBTransactionResult), DBTransactionResult.Error);
            return JsonConvert.SerializeObject(response);
        }

        public ActionResult UserLogOut()
        {
            if (Session[SessionKeys.user] != null)
                Session.Remove(SessionKeys.user);
            return RedirectToAction("Index", "home");
        }

        public ActionResult GetOrders()
        {
            var orders = OrderUtility.GetOrdersByUser(CurrentUser.Email);
            return View(orders);
        }
        

        public ActionResult password()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePasswordAction(ChangePassWordViewModel param)
        {
            if (!this.ModelState.IsValid)
            {
                return View(param);
            }
            else
            {
                if (UserUtility.IsMember(CurrentUser.Email, param.OldPassWord))
                {
                    UserUtility.ChangePassword(CurrentUser.Email, param.OldPassWord, param.NewPassWord);
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError("", "پسورد فعلی را نامعتبر می باشد. پسورد صحیح را وارد نمایید.");
            }
            return View(param);
        }
        public ActionResult ProfileEdit()
        {
            var user=UserUtility.GetUserDate(CurrentUser.Email);
            var profileViewModel = new ProfileEditViewModel();
            profileViewModel.FName = user.FName;
            profileViewModel.LName = user.LName;
            profileViewModel.Tel = user.Tel;
            profileViewModel.Mobile = user.Mobile;
            return View(profileViewModel);
        }
        [HttpPost]
        public ActionResult ChangeProfileAction(ProfileEditViewModel newProfile)
        {
            UserUtility.EditProfile(CurrentUser.Email, newProfile);
            return RedirectToAction("Index"); ;
        }

        public ActionResult Orders()
        {
            var orders = OrderUtility.GetOrdersByUser(CurrentUser.Email);
            return View(orders);
        }

        public ActionResult GetAllTransactions()
        {
            var orders = OrderUtility.GetAllTransactionsByUser(CurrentUser.Email);
            var transactions = new List<TransactionViewModel>();
            foreach (var order in orders)
            {
                var t = new TransactionViewModel
                {
                    Amount = order.Amount,
                    FactorId = order.FactorId,
                    PayDateTime = Convert.ToDateTime(order.SubmitDateTime),
                    RefernceNumber = order.TransactionRefNumber,
                    State = (PaymentState)Enum.Parse(typeof(PaymentState), order.PaymentState)
                };
                transactions.Add(t);
            }
            return View(transactions);
        }


    }
}