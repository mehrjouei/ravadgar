﻿using Newtonsoft.Json;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace RavadGarWebSite.Controllers
{
    [HandleError(ExceptionType = typeof(Exception), View = "~/Error/Index")]
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index(int id)
        {
            var news = NewsUtility.GetNews(id);
            return View(news);
        }
        public ActionResult All()
        {
            var news = NewsUtility.getAllNews();
            return View(news);
        }
        public ActionResult GetLastNews(int top)
        {
            var res = NewsUtility.getTopNews(top);
            List<NewsViewModel> resVM = new List<NewsViewModel>();
            foreach (var n in res)
            {
                resVM.Add(NewsViewModel.ConvertNewsToNewsViewModel(n));
            }
            return View(resVM);
        }
        public ActionResult GetLastNewsCol(int top)
        {
            var res = NewsUtility.getTopNews(top);
            return PartialView(res);
        }
        public ActionResult GetNews(int id = 1)
        {
            var news = NewsUtility.GetNews(id);
            if (news == null)
                return View(new NewsViewModel() { NewsTitle = "خبر مورد نظر موجو نیست." });
            var newsModel = NewsViewModel.ConvertNewsToNewsViewModel(news);

            return View(newsModel);
        }

        public ActionResult List(int id = 1)
        {
            List<News> res = new List<News>();
            int count = 10;
            int low = (id - 1) * count;
            int high = id * count;
            try
            {
                res = NewsUtility.getNewsIn(low, high);
            }
            catch (Exception)
            {

            }

            ViewBag.NewsCount = (int)NewsUtility.GetNewsCount();
            ViewBag.PageNewsCount = count;
            return View(res);
        }


    }
}