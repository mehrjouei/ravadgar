﻿using CaptchaMvc.Attributes;
using RavadGarWebSite.Framwork.Attributes;
using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RavadGarWebSite.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Admin a)
        {
            if (AdminUtility.IsMember(a))
            {
                AdminUtility.SetLastLoginDateTime(a);
                Session.Add(SessionKeys.admin, a);
                return RedirectToAction("Index", "Admin");
            }
            return View();
        }
        [Permission(Permissions.admin)]
        public ActionResult adminLogOut()
        {
            if(Session[SessionKeys.admin]!=null)
            Session.Remove(SessionKeys.admin);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Login()
        {
            return View();
        }

        [CaptchaVerify("error",ErrorMessage ="کد انسان یاب را به طور صحیج وارد نمایید.")]
        public ActionResult userlogin(string username, string password)
        {
            if (ModelState.IsValid)// مدل captcha
            {
                var user = UserUtility.GetUserDate(username, password);
                if (user!=null)
                {
                    Session.Add(SessionKeys.user, user);

                    if (Session[SessionKeys.ComeBackURL] != null)
                        return Redirect(Session[SessionKeys.ComeBackURL].ToString());
                    else
                        return Redirect("~/user/Index");
                }
                else
                {
                    TempData["InvalidUP"] = "اطلاعات حساب کاربری شما صحیح نمی باشد.";
                    return Redirect("~/Login/login");
                }
            }
            else
            {
                TempData["CaptchaError"] = "کد انسان یاب را به طور صحیج وارد نمایید.";
                return Redirect("~/Login/login");
            }
        }
        

    }
}