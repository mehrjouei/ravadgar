﻿using Newtonsoft.Json;
using RavadGarWebSite.Framwork.Attributes;
using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace RavadGarWebSite.Controllers
{
    [Permission(Permissions.admin)]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult pages()
        {
            return View();
        }
        public ActionResult AddPage()
        {
            return View();
        }
        public ActionResult EditPage(int ID)
        {
            var page = PageUtility.GetPage(ID);
            return View(page);
        }
        public ActionResult Users()
        {
            return View();
        }
        public ActionResult Settings()
        {
            return View();
        }
        public ActionResult categories()
        {
            return View();
        }
        public ActionResult Products()
        {
            return View();
        }
        public ActionResult news()
        {
            return View();
        }
        public ActionResult AddNews()
        {
            return View();
        }

        public ActionResult articles()
        {
            return View();
        }
        public ActionResult AddArticle()
        {
            return View();
        }
        public ActionResult Orders()
        {
            return View();
        }
        public ActionResult ProductsList()
        {
            return View();
        }
        public ActionResult AddProduct()
        {
            return View();
        }
        
        public ActionResult Factor(string factorId)
        {
            var res = OrderUtility.GetOrderById(factorId);
            return View(res);

        }

        #region product
        [HttpPost]
        public string AddProduct(Product p)
        {
            p.AddDateTime = DateTime.Now;
            DBTransactionResult res;
            if (ProductUtility.GetProduct(p.PCode) != null)
                res = ProductUtility.UpdateProduct(p);
            else
                res = ProductUtility.AddProduct(p);
            return JsonConvert.SerializeObject(new ResponseClass()
            {
                result = Enum.GetName(typeof(DBTransactionResult), res)
            });
        }
        [HttpPost]
        public string DeleteProduct(int pCode)
        {
            DBTransactionResult res;

                res = ProductUtility.DeleteProduct(pCode);
                //Removing pics.
                var images = ProductUtility.GetImages(pCode);
                foreach (var i in images)
                {
                    try
                    {
                        var path = Server.MapPath(string.Format("~/productimages/Images/{0}", i.ImageLink));
                        ImageUtility.RemoveImage(path);
                        path = Server.MapPath(string.Format("~/productimages/ThumbNail/{0}", i.ImageLink));
                        ImageUtility.RemoveImage(path);
                    }
                    catch (Exception)
                    {
                    }

                }

            return JsonConvert.SerializeObject(new ResponseClass() { result = Enum.GetName(typeof(DBTransactionResult), res) });
        }
        [HttpPost]
        public string AddNews(News n)
        {
            n.InsertDateTime = DateTime.Now;
            var res = NewsUtility.AddNews(n);
            return JsonConvert.SerializeObject(new ResponseClass()
            {
                result = Enum.GetName(typeof(DBTransactionResult), res)
            });
        }
        [HttpPost]
        public string AddArticle(Article n)
        {
            n.InsertDateTime = DateTime.Now;
            var res = ArticleUtility.AddArticle(n);
            return JsonConvert.SerializeObject(new ResponseClass()
            {
                result = Enum.GetName(typeof(DBTransactionResult), res)
            });
        }
        public string GetNews(int page, int rows, string sidx, string sord)
        {
            var news = NewsUtility.GetAllNewsIn(page, rows, sidx, sord);
            if (news.records > 0)
            {
                return JsonConvert.SerializeObject(news);
            }
            else
            {
                TempData["NothingUser"] = "لیست اخبار خالی است.";
                return null;
            }
        }
        public string GetArticles(int page, int rows, string sidx, string sord)
        {
            var news = ArticleUtility.GetAllArticlesIn(page, rows, sidx, sord);
            if (news.records > 0)
            {
                return JsonConvert.SerializeObject(news);
            }
            else
            {
                TempData["NothingUser"] = "لیست اخبار خالی است.";
                return null;
            }
        }
        public string GetNewsCategories()
        {
            var news = NewsCategoryUtility.GetAllCategories();
            if (news.Count > 0)
            {
                return JsonConvert.SerializeObject(news);
            }
            else
            {
                TempData["NothingUser"] = "لیست اخبار خالی ایت.";
                return null;
            }
        }
        public string GetArticlesCategories()
        {
            var articles = ArticleCategoryUtility.GetAllCategories();
            if (articles.Count > 0)
            {
                return JsonConvert.SerializeObject(articles);
            }
            else
            {
                TempData["NothingUser"] = "لیست موضوع مقالات خالی است.";
                return null;
            }
        }
        public string GetProducts(int page, int rows, string sidx, string sord)
        {
            var products = ProductUtility.GetAllProductsIn(page, rows, sidx, sord);
            if (products.records > 0)
            {
                return JsonConvert.SerializeObject(products);
            }
            else
            {
                TempData["NothingUser"] = "لیست کاربران عضو شده در سامانه خالی است.";
                return null;
            }
        }
        public ActionResult ProductEdit(int PCode)
        {
            var product = ProductUtility.GetProduct(PCode);
            return View(product);
        }
        [HttpPost]
        public string EditProductDetail(Product p)
        {
            throw new NotImplementedException();
        }
        [HttpPost]
        public string RemoveImage(ProductImage image)
        {
            string imageName = image.ImageLink;
            var res = ProductUtility.RemoveImage((int)image.Pcode, image.ID);
            if (res == DBTransactionResult.Successful)
            {
                var path = Server.MapPath(string.Format("~/productimages/Images/{0}", imageName));
                ImageUtility.RemoveImage(path);
                path = Server.MapPath(string.Format("~/productimages/ThumbNail/{0}", imageName));
                ImageUtility.RemoveImage(path);
                //May be require some log system.
            }
            return JsonConvert.SerializeObject(
                new ResponseClass()
                {
                    result = Enum.GetName(typeof(DBTransactionResult), res)
                });
        }
        
        #endregion product

        #region settings
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Settings(ChangePassWordViewModel changePass)
        {
            if (!ModelState.IsValid)
            {
                return View(changePass);
            }
            if (changePass.ReNewPassWord != changePass.ReNewPassWord)
            {
                return View(changePass);
            }
            AdminUtility.ChangePassWord("admin", changePass.OldPassWord, changePass.NewPassWord);
            return RedirectToAction("Settings");
        }
        #endregion settings

        #region uploader
        public ActionResult uploader(int? chunk, string name)
        {
            var fileUpload = Request.Files[0];
            var uploadPath = Server.MapPath("~/productfiles/files");
            chunk = chunk ?? 0;
            using (var fs = new FileStream(Path.Combine(uploadPath, name), chunk == 0 ? FileMode.Create : FileMode.Append))
            {
                var buffer = new byte[fileUpload.InputStream.Length];
                fileUpload.InputStream.Read(buffer, 0, buffer.Length);
                fs.Write(buffer, 0, buffer.Length);
            }
            return Content("chunk uploaded", "text/plain");
        }
        public string ProductImageUploader(int? chunk, string name)
        {
            var rndName = StringUtility.GenerateRndString();
            rndName = string.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(name), rndName, Path.GetExtension(name));
            var fileUpload = Request.Files[0];
            var uploadPath = Server.MapPath("~/productimages");
            Directory.CreateDirectory(uploadPath);
            Directory.CreateDirectory(uploadPath+ "//ThumbNail");
            Directory.CreateDirectory(uploadPath+ "//Images");
            chunk = chunk ?? 0;
            using (var fs = new FileStream(Path.Combine(uploadPath, rndName), chunk == 0 ? FileMode.Create : FileMode.Append))
            {
                var buffer = new byte[fileUpload.InputStream.Length];
                fileUpload.InputStream.Read(buffer, 0, buffer.Length);
                fs.Write(buffer, 0, buffer.Length);
            }
            ProductUtility.AddImagetoTemp(new uploadedImage()
            {
                ImageName = rndName
            });
            int id = ProductUtility.GetTempImageID(rndName);
            //return Content("chunk uploaded", "text/plain");
            //var im = new Bitmap(Server.MapPath("~/productimages/" + rndName));
            //for thumbnail
            ImageResizer(Server.MapPath("~/productimages/ThumbNail/" + rndName), rndName, 200, 152);
            //for real;
            ImageResizer(Server.MapPath("~/productimages/Images/" + rndName), rndName, 700, 700);
            //Delete first image.
            foreach (var file in Directory.GetFiles(Server.MapPath("~/productimages")))
            {
                while (System.IO.File.Exists(file))
                    try { System.IO.File.Delete(file); }
                    catch { }
            }
            return id.ToString();
        }
        public string NewsImageUploader(int? chunk, string name)
        {
            var rndName = StringUtility.GenerateRndString();
            rndName = string.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(name), rndName, Path.GetExtension(name));
            var fileUpload = Request.Files[0];
            var uploadPath = Server.MapPath("~/productimages");
            chunk = chunk ?? 0;
            using (var fs = new FileStream(Path.Combine(uploadPath, rndName), chunk == 0 ? FileMode.Create : FileMode.Append))
            {
                var buffer = new byte[fileUpload.InputStream.Length];
                fileUpload.InputStream.Read(buffer, 0, buffer.Length);
                fs.Write(buffer, 0, buffer.Length);
            }
            ProductUtility.AddImagetoTemp(new uploadedImage()
            {
                ImageName = rndName
            });
            int id = ProductUtility.GetTempImageID(rndName);
            //return Content("chunk uploaded", "text/plain");
            //var im = new Bitmap(Server.MapPath("~/productimages/" + rndName));
            //for thumbnail
            ImageResizer(Server.MapPath("~/uploadedimages/news/ThumbNail/" + rndName), rndName, 200, 152);
            //for real;
            ImageResizer(Server.MapPath("~/uploadedimages/news/Images/" + rndName), rndName, 700, 700);
            //Delete first image.
            foreach (var file in Directory.GetFiles(Server.MapPath("~/productimages")))
            {
                while (System.IO.File.Exists(file))
                    try { System.IO.File.Delete(file); }
                    catch { }
            }
            return id.ToString();
        }
        public string ArticleImageUploader(int? chunk, string name)
        {
            var rndName = StringUtility.GenerateRndString();
            rndName = string.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(name), rndName, Path.GetExtension(name));
            var fileUpload = Request.Files[0];
            var uploadPath = Server.MapPath("~/productimages");
            chunk = chunk ?? 0;
            using (var fs = new FileStream(Path.Combine(uploadPath, rndName), chunk == 0 ? FileMode.Create : FileMode.Append))
            {
                var buffer = new byte[fileUpload.InputStream.Length];
                fileUpload.InputStream.Read(buffer, 0, buffer.Length);
                fs.Write(buffer, 0, buffer.Length);
            }
            ProductUtility.AddImagetoTemp(new uploadedImage()
            {
                ImageName = rndName
            });
            int id = ProductUtility.GetTempImageID(rndName);
            //return Content("chunk uploaded", "text/plain");
            //var im = new Bitmap(Server.MapPath("~/productimages/" + rndName));
            //for thumbnail
            ImageResizer(Server.MapPath("~/uploadedimages/articles/ThumbNail/" + rndName), rndName, 200, 152);
            //for real;
            ImageResizer(Server.MapPath("~/uploadedimages/articles/Images/" + rndName), rndName, 700, 700);
            //Delete first image.
            foreach (var file in Directory.GetFiles(Server.MapPath("~/productimages")))
            {
                while (System.IO.File.Exists(file))
                    try { System.IO.File.Delete(file); }
                    catch { }
            }
            return id.ToString();
        }
        private void ImageResizer(string path, string imageName, int width, int height)
        {
            using (var sourceImage = CodeCarvings.Piczard.ImageArchiver.LoadImage("~/productimages/" + imageName))
            {
                if (sourceImage.Size.Width == sourceImage.Size.Height)
                {
                    // Just copy the file
                    System.IO.File.Copy(Server.MapPath("~/productimages/" + imageName), path);
                }
                else
                {
                    // Calculate the square size
                    int imageSize = sourceImage.Size.Width > sourceImage.Size.Height ? sourceImage.Size.Width : sourceImage.Size.Height;
                    // Get a fixed resize filter (square)
                    var filter = new CodeCarvings.Piczard.FixedResizeConstraint(width, height);

                    // Force white background
                    filter.CanvasColor = CodeCarvings.Piczard.BackgroundColor.GetStatic(System.Drawing.Color.White);

                    // Process the image (output = JPEG - 82% quality)
                    filter.SaveProcessedImageToFileSystem(sourceImage, path, new CodeCarvings.Piczard.JpegFormatEncoderParams(82));
                    if (sourceImage.Size.Height > sourceImage.Size.Width)
                    {
                        using (var image = new Bitmap(path))
                        {
                            image.RotateFlip(RotateFlipType.Rotate90FlipX);
                            image.Save(path);
                        }
                    }
                }
            }
        }
        #endregion uploader

        #region category
        [HttpPost]
        public string AddCategory(Category cat)
        {
            var res = CategoryUtility.AddItemToCategories(cat);
            return JsonConvert.SerializeObject(new ResponseClass()
            {
                result = Enum.GetName(typeof(DBTransactionResult), res)
            });
        }
        [HttpPost]
        public string EditCategory(Category cat)
        {
            int id = cat.CategoryID;
            int parentId = (int)cat.ParentCategoryID;
            string newValue = cat.CategoryName;
            var res = CategoryUtility.EditCategoryById(newValue, id, parentId);
            return JsonConvert.SerializeObject(new ResponseClass()
            {
                result = Enum.GetName(typeof(DBTransactionResult), res)
            });
        }
        public string GetCategories()
        {
            var cats = CategoryUtility.GetAllCategories();
            var res = cats.Select(s => new { s.CategoryID, s.CategoryName, s.ParentCategoryID });
            if (cats != null)
                return JsonConvert.SerializeObject(res);
            else
                return JsonConvert.SerializeObject(new ResponseClass() { result = "هیج دسته بندی پیدا نشد" });
        }
        #endregion category

        #region user
        public string GetUsers(int page, int rows, string sidx, string sord)
        {
            var users = UserUtility.GetAllUsersIn(page, rows, sidx, sord);
            if (users.records > 0)
            {
                return JsonConvert.SerializeObject(users);
            }
            else
            {
                TempData["NothingUser"] = "لیست کاربران عضو شده در سامانه خالی ایت.";
                return null;
            }
        }
        [HttpPost]
        public ActionResult EditUsers(string email)
        {
            Session.Add(SessionKeys.ComeBackURL, Request.UrlReferrer.ToString());
            if (string.IsNullOrEmpty(email))
            {
                TempData["NotMember"] = "کاربری با مشخصات وارد شده موجود نمی باششد.";
                return View(new Models.User());
            }

            var user = UserUtility.GetUserDate(email);
            return View(user);
        }
        #endregion user

        #region page
        [HttpPost]
        public string AddPage(Models.Page p)
        {
            var res = PageUtility.AddPage(p);
            return JsonConvert.SerializeObject(new ResponseClass()
            {
                result = Enum.GetName(typeof(DBTransactionResult), res)
            });
        }
        [HttpPost]
        public string EditPage(Models.Page p)
        {
            var res = PageUtility.EditPage(p);
            return JsonConvert.SerializeObject(new ResponseClass()
            {
                result = Enum.GetName(typeof(DBTransactionResult), res)
            });
        }
        public string GetPages(int page, int rows, string sidx, string sord)
        {
            var pages = PageUtility.GetAllPagesIn(page, rows, sidx, sord);
            if (pages.records > 0)
            {
                return JsonConvert.SerializeObject(pages);
            }
            else
            {
                TempData["NothingUser"] = "لیست کاربران عضو شده در سامانه خالی است.";
                return null;
            }
        }
        #endregion page

        #region orders
        public string GetAllOrders(int page, int rows, string sidx, string sord)
        {
            MyGridData orders= OrderUtility.GetAllOrders(page,rows,sidx, sord);
            if (orders.records > 0)
            {
                return JsonConvert.SerializeObject(orders);
            }
            else
            {
                TempData["NoOrder"] = "هیچ سفارشی در سیستم سفارش ثبت نشده است";
                return null;
            }
        }
        
        public ActionResult GetAllTransactions()
        {
            return View();
        }
        #endregion

        
    }
}