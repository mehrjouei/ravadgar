﻿using RavadGarWebSite.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RavadGarWebSite.Controllers
{
    public class PageController : Controller
    {
        // GET: Page
        public ActionResult Index(string Name)
        {
            var page = PageUtility.GetPageByName(Name);
            return View(page);
        }
    }
}