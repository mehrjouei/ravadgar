﻿using Newtonsoft.Json;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models;
using RavadGarWebSite.Models.DAL;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RavadGarWebSite.Controllers
{
    //[HandleError(ExceptionType = typeof(Exception), View = "~/Error/Index")]
    public class ArticleController : Controller
    {
        [HttpGet]
        public ActionResult Index(int? id)
        {
            var news = ArticleUtility.GetArticle((int)id);
            return View(news);
        }

        public ActionResult GetLastArticles(int top)
        {
            var res = ArticleUtility.getTopArticles(top);
            List<ArticleViewModel> resVM = new List<ArticleViewModel>();
            foreach (var n in res)
            {
                resVM.Add(ArticleViewModel.ConvertArticleToArticleViewModel(n));
            }
            return View(resVM);
        }
        public ActionResult GetLastArticlesCol(int top)
        {
            var res = ArticleUtility.getTopArticles(top);
            return PartialView(res);
        }
        public ActionResult GetArticle(int id = 1)
        {
            var a = ArticleUtility.GetArticle(id);
            if (a == null)
                return View(new ArticleViewModel() { ArticleTitle = "مقاله مورد نظر موجو نیست." });
            var AModel = ArticleViewModel.ConvertArticleToArticleViewModel(a);

            return View(AModel);
        }

        [HttpGet]
        public ActionResult List(int page=1)
        {
            int count = 10;
            int low = (page-1) * count;
            int high = page * count;
            List<Article> res = ArticleUtility.getArticlesIn(low, high);
            ViewBag.ArticleCount = (int)ArticleUtility.GetArticleCount();
            ViewBag.PageArticleCount = count;
            return View(res);
        }

    }
}