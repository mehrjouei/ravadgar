﻿using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class NewsViewModel
    {
        public long NewsCode { get; set; }
        public string NewsTitle { get; set; }
        public string Summary { get; set; }
        public string NewsContent { get; set; }
        public string Image { get; set; }
        public string Category { get; set; }
        public int Visit { get; set; }
        public string InsertDateTime { get; set; }

        public static NewsViewModel ConvertNewsToNewsViewModel(News n)
        {
            var newsVM = new NewsViewModel();
            newsVM.Category = n.Category;
            newsVM.Image = n.Image;
            newsVM.InsertDateTime = DateTimeUtility.ConvertDatetimeToStringShmasi(n.InsertDateTime);
            newsVM.NewsCode = n.NewsCode;
            newsVM.NewsContent = n.NewsContent;
            newsVM.NewsTitle = n.NewsTitle;
            newsVM.Summary = n.Summary;
            newsVM.Visit = (int)n.Visit;
            return newsVM;
        }
    }
}