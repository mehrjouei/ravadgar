﻿using RavadGarWebSite.Framework.BaseClasses.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class TransactionViewModel
    {
        public string FactorId { set; get; }
        public string RefernceNumber { set; get; }
        public DateTime PayDateTime { set; get; }
        public long Amount { set; get; }
        public PaymentState State { set; get; }
    }
}