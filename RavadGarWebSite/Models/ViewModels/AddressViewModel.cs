﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class AddressViewModel
    {
        public long Id { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Mobile { get; set; }
        public string Tel { get; set; }
        public string Customer { get; set; }

        public Address ConvertToAddress()
        {
            var address = new Address();
            address.City = this.City;
            address.State = this.State;
            address.Address1 = this.Address;
            address.Id = this.Id;
            address.Mobile = this.Mobile;
            address.PostalCode = this.PostalCode;
            address.Tel = this.Tel;
            return address;
        }
        public static AddressViewModel ConvertToAddressViewModel(Address add)
        {
            var address = new AddressViewModel();
            address.City = add.City;
            address.State = add.State;
            address.Address = add.Address1;
            address.Mobile = add.Mobile;
            address.PostalCode = add.PostalCode;
            address.Tel = add.Tel;
            return address;
        }
    }
}