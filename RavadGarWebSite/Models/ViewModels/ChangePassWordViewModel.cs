﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class ChangePassWordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name ="پسورد فعلی")]
        public string OldPassWord { set; get; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name ="پسورد جدید")]
        public string NewPassWord { set; get; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name ="تکرار پسورد جدید")]
        [Compare("NewPassWord", ErrorMessage ="پسورد جدید و تکرار پسورد جدید باید یکسان باشد")]
        public string ReNewPassWord { set; get; }
    }
}