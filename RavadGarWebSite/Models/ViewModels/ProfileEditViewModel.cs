﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class ProfileEditViewModel
    {
        public string FName { set; get; }
        public string LName { set; get; }
        public string Tel { set; get; }
        public string Mobile { set; get; }
    }
}