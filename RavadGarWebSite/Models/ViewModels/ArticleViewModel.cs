﻿using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class ArticleViewModel
    {
        public int ArticleCode { get; set; }
        public string ArticleTitle { get; set; }
        public string ArticleSummary { get; set; }
        public string ArticleContetnt { get; set; }
        public string Image { get; set; }
        public string Category { get; set; }
        public Nullable<int> Visits { get; set; }
        public Nullable<int> Score { get; set; }
        public string InsertDateTime { get; set; }


        public static ArticleViewModel ConvertArticleToArticleViewModel(Article a)
        {
            ArticleViewModel avm = new ArticleViewModel();
            avm.Visits = a.Visit;
            avm.InsertDateTime = DateTimeUtility.ConvertDatetimeToStringShmasi(a.InsertDateTime);
            avm.Image = a.Image;
            avm.Category = a.Category;
            avm.ArticleTitle = a.ArticleTitle;
            avm.ArticleSummary = a.Summary;
            avm.ArticleContetnt = a.ArticleContent;
            avm.ArticleCode = a.ArticleCode;
            return avm;
        }
    }

}