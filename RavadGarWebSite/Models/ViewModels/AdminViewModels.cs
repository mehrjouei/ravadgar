﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class AdminViewModels
    {
    }
   
    public class FeatureViewModel
    {
        public int ID { get; set; }
        public string FeatureFName { get; set; }
        public string FeatureEName { get; set; }
        public Nullable<int> ProductCode { get; set; }
        public virtual Product Product { get; set; }
    }
    public class ImageViewModel
    {
        public int ID { get; set; }
        public string ImageText { get; set; }
        public string ImageLink { get; set; }
        public Nullable<int> Pcode { get; set; }
        public virtual Product Product { get; set; }
    }
}