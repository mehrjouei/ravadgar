﻿using RavadGarWebSite.Framework.BaseClasses.Enum;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class OrderViewModel
    {
        public int ProductId { set; get; }
        public long? CombinationId { set; get; }
        public int Number { set; get; }
        public long Price { set; get; }
        public string ProductName { set; get;}
        public string CombinationName { set; get; }
        public string CombinationValue { set; get; }



        public static OrderViewModel Convert(OrderDetail orderDetail)
        {
            var order = new OrderViewModel();
            order.CombinationId = orderDetail.CombinationId;
            order.Number = (int)orderDetail.Number;
            order.Price = (long)orderDetail.Price;
            order.ProductId = orderDetail.Product.PCode;
            order.ProductName = orderDetail.Product.PFName;
            if (orderDetail.ProductCombination != null)
            {
                order.CombinationName = orderDetail.ProductCombination.CombinationName;
                order.CombinationValue = orderDetail.ProductCombination.CombinationValue;
            }
            return order;

        }
    }

    public class FactorViewModel
    {
        public List<OrderViewModel> Orders { set; get; }
        public long Amount { set; get; }
        public string FactorId { set; get; }
        public PaymentState PaymentState { set; get; }
        public long ShippingFee { set; get; }
        public string UserEmail { set; get; }
        public string SubmitDateTime { set; get; }

        internal static FactorViewModel MakeOrder(List<OrderViewModel> products)
        {
            var factor = new FactorViewModel();
            factor.Orders = products;
            factor.FactorId = Framwork.BaseClasses.Utilities.StringUtility.GenerateSimpleRndString();
            var x=ProductUtility.GetProductDetailsById(products);
            foreach (var prod in factor.Orders)
            {
                var product = x[prod];
                if(product.ProductCombinations!=null && product.ProductCombinations.Count>0)
                    prod.Price = (long)product.ProductCombinations.FirstOrDefault(f=>f.Id==prod.CombinationId).CombinationPrice;
                else
                    prod.Price = (long)product.Price;
            }
            factor.Amount = factor.Orders.Sum(s => s.Price * s.Number);
            factor.PaymentState =PaymentState.NotPayed;
            return factor;
        }

        internal static FactorViewModel MakeFactor(Order order)
        {
            var factor = new FactorViewModel();
            factor.Amount = (long)order.Amount;
            factor.FactorId = order.FactorId;
            factor.Orders = new List<OrderViewModel>();
            factor.SubmitDateTime = DateTimeUtility.ConvertDatetimeToStringShmasi(order.SubmitDateTime,'-');
            foreach (var orderDetail in order.OrderDetails)
            {
                factor.Orders.Add(OrderViewModel.Convert(orderDetail));
            }
            factor.PaymentState =(PaymentState)Enum.Parse(typeof(PaymentState), order.PaymentState);
            factor.ShippingFee = (long)order.ShippingFee;
            factor.UserEmail = order.UserEmail;
            return factor;
        }
    }
}