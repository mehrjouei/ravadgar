﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public static class ViewModelsToModelsUtility
    {
        public static Product ProductViewModelToProduct(ProductViewModel pvm)
        {
            Product prod = new Product();
            
            prod.Description = pvm.Description;
            prod.FullDescription = pvm.FullDescription;
            prod.Image = pvm.Image;
            prod.Number = pvm.Number;
            prod.PEName = pvm.PEName;
            prod.PFName = pvm.PFName;
            prod.Price = pvm.Price;
            return prod;
        }
    }
}