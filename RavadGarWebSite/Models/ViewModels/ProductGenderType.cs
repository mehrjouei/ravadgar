﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public enum ProductGenderType
    {
        Male,
        Female,
        Both
    }
}