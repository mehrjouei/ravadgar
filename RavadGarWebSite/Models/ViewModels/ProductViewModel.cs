﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.ViewModels
{
    public class ProductViewModel
    {
        public string PEName { get; set; }
        public string PFName { get; set; }
        public string Description { get; set; }
        public Nullable<long> Price { get; set; }
        public string Image { get; set; }
        public string FullDescription { get; set; }
        public Nullable<int> UserScore { get; set; }
        public Nullable<int> Category { get; set; }
        public Nullable<int> Number { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<int> Visits { get; set; }

        public List<ProductImage> Images { set; get; }
        
    }
}