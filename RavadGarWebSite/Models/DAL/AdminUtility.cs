﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class AdminUtility
    {
        public static DBTransactionResult ChangePassWord(string userName, string oldPass, string newPass)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                string hPass = AccountingUtility.GetSecurePassword(oldPass);
                var admin=entity.Admins.FirstOrDefault(a => a.UserName == userName && a.PassWord == hPass);
                admin.PassWord = AccountingUtility.GetSecurePassword(newPass);
                var res=entity.SaveChanges();
                if (res > 0)
                    return DBTransactionResult.Successful;
                else
                    return DBTransactionResult.Error;
            } 
        }

        internal static void SetLastLoginDateTime(Admin a)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                try
                {
                    var h = AccountingUtility.GetSecurePassword(a.PassWord);
                    var admin = entity.Admins.FirstOrDefault(f => f.UserName == a.UserName && f.PassWord == h);
                    admin.LastView = DateTime.Now;
                    var res = entity.SaveChanges();
                }
                catch { }
            }

        }

        internal static bool IsMember(Admin a)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var h = AccountingUtility.GetSecurePassword(a.PassWord);
                var res=entity.Admins.FirstOrDefault(f => f.UserName == a.UserName && f.PassWord == h);
                if (res != null)
                    return true;
                else
                    return false;
            }
        }
    }
}