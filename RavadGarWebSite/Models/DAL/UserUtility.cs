﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RavadGarWebSite.Models.ViewModels;

namespace RavadGarWebSite.Models.DAL
{
    public static class UserUtility
    {
        public static void AddUser(User user)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var address = new Address();
                user.Addresses.Add(address);
                entity.Users.Add(user);
                entity.SaveChanges();
            }
        }
        public static User GetUserDate(string email)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var user = entity.Users.FirstOrDefault(u => u.Email == email);
                return user;
            }
        }
        public static bool IsMember(string email, string password)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var hashPass = AccountingUtility.GetSecurePassword(password);
                var user = entity.Users.FirstOrDefault(u => u.Email == email && u.PassWord == hashPass);
                if (user != null)
                    return true;
                else
                    return false;
            }
        }
        public static bool IsRegisteredEmail(string email)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var user=entity.Users.FirstOrDefault(f => f.Email == email);
                if (user == null)
                    return false;
                else
                    return true;
            }
        }

        internal static User GetUserDate(string email, string password)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var hPass = AccountingUtility.GetSecurePassword(password);
                var user = entity.Users.FirstOrDefault(u => u.Email == email && u.PassWord== hPass);
                return user;
            }
        }

        public static void SetLastVisit(string email)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var user=entity.Users.FirstOrDefault(f => f.Email == email);
                user.LastVisit = DateTime.Now;
                entity.SaveChanges();
            }
        }
        public static List<User> GetUsers()
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var users = entity.Users.ToList();
                return users;
            }
        }

        public static MyGridData GetAllUsersIn(int page, int rows, string sidx, string sord)
        {
            MyGridData myData = new MyGridData();
            myData.page = page;

            using (var entity = RepositoryUtil.OpenRepository())
            {
                var users = entity.Users.ToList();
                myData.total = (int)Math.Ceiling(users.Count / (double)rows);
                myData.records = users.Count;
                switch (sidx)
                {
                    case "Email":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.Email, b.Email));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.Email, b.Email));
                        break;
                    case "Name":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.FName, b.FName));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.LName, b.LName));
                        break;
                    case "LName":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.LName, b.LName));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.LName, b.LName));
                        break;
                    case "Mobile":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.Mobile, b.Mobile));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.Mobile, b.Mobile));
                        break;
                    case "Credit":
                        if (sord == "asc")
                            users.Sort((a, b) => decimal.Compare((decimal)a.Credit, (decimal)b.Credit));
                        else
                            users.Sort((a, b) => -1 * decimal.Compare((decimal)a.Credit, (decimal)b.Credit));
                        break;
                    case "JoinDateTime":
                    default:
                        if (sord == "asc")
                            users.Sort((a, b) => DateTime.Compare((DateTime)a.JoinDateTime, (DateTime)b.JoinDateTime));
                        else
                            users.Sort((a, b) => -1 * DateTime.Compare((DateTime)a.JoinDateTime, (DateTime)b.JoinDateTime));
                        break;
                }
                users = users.GetRange((page - 1) * rows, Math.Min(users.Count - ((page - 1) * rows), rows));
                var res = users.Select(s => new { s.Email, s.FName, s.LName, s.Mobile, s.Credit, s.JoinDateTime }).ToList();
                myData.rows = res;

                return myData;
            }
        }

        internal static void EditProfile(string email, ProfileEditViewModel newProfile)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var user=entity.Users.FirstOrDefault(f => f.Email == email);
                user.FName = newProfile.FName;
                user.LName = newProfile.LName;
                user.Tel = newProfile.Tel;
                user.Mobile = newProfile.Mobile;
                entity.SaveChanges();
            }
        }

        public static void ChangePassword(string email, string oldPass, string newPass)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var oldPassword = AccountingUtility.GetSecurePassword(oldPass);
                var user=entity.Users.FirstOrDefault(f => f.Email ==email && f.PassWord== oldPassword);
                if(user!=null)
                {
                    user.PassWord = AccountingUtility.GetSecurePassword(newPass);
                }
                entity.SaveChanges();
            }
        }
    }
}