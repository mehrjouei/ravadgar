﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class SlidShowUtility
    {
        public static DBTransactionResult AddSlideShow(SlideShow ss)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                entity.SlideShows.Add(ss);
                var res=entity.SaveChanges();
                if (res > 0)
                    return DBTransactionResult.Successful;
                else
                    return DBTransactionResult.Error;

            }
        }
        public static List<SlideShow> GetSlideShows()
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var sss=entity.SlideShows.ToList();
                return sss;
            }
        }
    }
}