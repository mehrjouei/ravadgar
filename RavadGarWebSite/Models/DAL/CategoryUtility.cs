﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class CategoryUtility
    {
        public static DBTransactionResult AddItemToCategories(Category category)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                try
                {
                    entity.Categories.Add(category);
                    entity.SaveChanges();
                    return DBTransactionResult.Successful;
                }
                catch
                {
                    return DBTransactionResult.Error;
                }
                    
            }
        }
        public static List<Category> GetAllCategories()
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var res = entity.Categories.ToList().OrderBy(c => c.ParentCategoryID).ToList();
                return res;
            }
        }
        public static DBTransactionResult EditCategoryById(string newValue, int id, int pId)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                try
                {
                    var cat = entity.Categories.FirstOrDefault(c => c.CategoryID == id);
                    if (cat != null)
                    {
                        cat.CategoryName = newValue;
                        cat.ParentCategoryID = pId;
                        var res = entity.SaveChanges();
                        if (res > 0)
                            return DBTransactionResult.Successful;
                        else
                            return DBTransactionResult.Error;
                    }
                }
                catch { }
            }
            return DBTransactionResult.Error;
        }
        public static DBTransactionResult EditCategoryByName(string newValue,string oldValue)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                try
                {
                    var cat = entity.Categories.FirstOrDefault(c => c.CategoryName == oldValue);
                    if (cat != null)
                    {
                        cat.CategoryName = newValue;
                        var res = entity.SaveChanges();
                        if (res > 0)
                            return DBTransactionResult.Successful;
                        else
                            return DBTransactionResult.Error;
                    }
                }
                catch { }
            }
            return DBTransactionResult.Error;
        }
        public static int GetCategoryId(string catName)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var cat=entity.Categories.FirstOrDefault(c => c.CategoryName == catName);
                if (cat != null)
                    return cat.CategoryID;
                else
                    return -1;
            }
        }
        public static int GetParentCategoryId(string catName)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var cat = entity.Categories.FirstOrDefault(c => c.CategoryName == catName);
                if (cat != null)
                    return (int)cat.ParentCategoryID;
                else
                    return -1;
            }
        }
    }
}