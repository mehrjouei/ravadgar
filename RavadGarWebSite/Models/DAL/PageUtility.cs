﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace RavadGarWebSite.Models.DAL
{
    public static class PageUtility
    {
        public static Page GetPage(int pCode)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var prod = entity.Pages.FirstOrDefault(p => p.ID == pCode);
                    return prod;
                }
                catch { }
            }
            return new Page();
        }
        public static Page GetPageByName(string name)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var prod = entity.Pages.FirstOrDefault(p => p.Name == name);
                    return prod;
                }
                catch { }
            }
            return new Page();
        }
        public static DBTransactionResult AddPage(Page p)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    entity.Pages.Add(p);
                    var res = entity.SaveChanges();
                    if (res > 0)
                        return DBTransactionResult.Successful;
                }
                catch {  }
            }
            return DBTransactionResult.Error;
        }
        /// <summary>
        /// Warning:: This Method should be fined, It's not Sure
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        internal static DBTransactionResult EditPage(Page p)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                try
                {
                    var page = entity.Pages.FirstOrDefault(f => f.ID == p.ID);
                    page.Name = p.Name;
                    page.PageContent = p.PageContent;
                    page.Title = p.Title;
                    page.Enable = p.Enable;
                    var res = entity.SaveChanges();
                    if (res > 0)
                        return DBTransactionResult.Successful;
                }
                catch { }
            }
            return DBTransactionResult.Error;
        }


        internal static MyGridData GetAllPagesIn(int page, int rows, string sidx, string sord)
        {
            MyGridData myData = new MyGridData();
            myData.page = page;

            using (var entity = RepositoryUtil.OpenRepository())
            {
                var users = entity.Pages.ToList();
                myData.total = (int)Math.Ceiling(users.Count / (double)rows);
                myData.records = users.Count;
                switch (sidx)
                {
                    case "ID":
                        if (sord == "asc")
                            users.Sort((a, b) => a.ID.CompareTo(b.ID));
                        else
                            users.Sort((a, b) => -1 * a.ID.CompareTo(b.ID));
                        break;
                    case "Name":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.Name, b.Name));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.Name, b.Name));
                        break;
                    case "Title":
                        if (sord == "asc")
                            users.Sort((a, b) => a.Title.CompareTo(b.Title));
                        else
                            users.Sort((a, b) => -1 * a.Title.CompareTo(b.Title));
                        break;
                }
                users = users.GetRange((page - 1) * rows, Math.Min(users.Count - ((page - 1) * rows), rows));
                var res = users.Select(s => new { s.ID, s.Name, s.Title}).ToList();
                myData.rows = res;
                return myData;
            }
        }
    }
}