﻿using PersionDate;
using RavadGarWebSite.Framework.BaseClasses.Enum;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class OrderUtility
    {
        public static List<Order> GetOrdersByUser(string email)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var orders=entity.Orders.Where(f=>f.UserEmail==email).ToList();
                foreach (var order in orders)
                {
                    order.OrderDetails = order.OrderDetails.ToList();
                    foreach (var prod in order.OrderDetails)
                    {
                        prod.Product = prod.Product as Models.Product;
                        prod.ProductCombination = prod.ProductCombination as Models.ProductCombination;
                    }
                }
                return orders;
            }
        }
        public static Order GetOrderById(string factorId)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var order=entity.Orders.FirstOrDefault(f => f.FactorId == factorId);
                order.OrderDetails = order.OrderDetails.ToList();
                foreach (var prod in order.OrderDetails)
                {
                    prod.Product = prod.Product as Models.Product;
                    prod.ProductCombination = prod.ProductCombination as Models.ProductCombination;
                }
                order.User = order.User as User;
                order.User.Addresses = order.User.Addresses.ToList();
                return order;
            }
        }
        public static Order GetOrderByAuthority(string authority)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var order = entity.Orders.FirstOrDefault(f => f.authority == authority);
                order.OrderDetails = order.OrderDetails.ToList();
                foreach (var prod in order.OrderDetails)
                {
                    prod.Product = prod.Product as Models.Product;
                    prod.ProductCombination = prod.ProductCombination as Models.ProductCombination;
                }
                order.User = order.User as User;
                order.User.Addresses = order.User.Addresses.ToList();
                return order;
            }
        }
        public static void AddOrder(FactorViewModel factor)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var order=entity.Orders.Create();
                order.FactorId = factor.FactorId;
                order.SubmitDateTime = DateTime.Now;
                order.PaymentState = Enum.GetName(typeof(PaymentState), factor.PaymentState);
                order.ShippingFee = (long)factor.ShippingFee;
                //باید از تعداد محصولات در انبار کسر گردد
                foreach (var o in factor.Orders)
                {
                    order.OrderDetails.Add(new OrderDetail()
                    {
                        Number = (int)o.Number,
                        Price = o.Price,
                        ProductCombination=o.CombinationId!=0? entity.ProductCombinations.FirstOrDefault(f=>f.Id==o.CombinationId):null,
                        Product=entity.Products.FirstOrDefault(f=>f.PCode==o.ProductId),
                        
                    });

                    if (o.CombinationId != 0)
                    {
                        var productCombination = entity.ProductCombinations.FirstOrDefault(f => f.Id == o.CombinationId);
                        if (productCombination != null)
                            productCombination.CombinationNumber -= (int)o.Number;
                    }
                    var product = entity.Products.FirstOrDefault(f => f.PCode == o.ProductId);
                    product.Number -= (int)o.Number;
                     
                }
                
                order.Amount = factor.Amount;
                order.User = entity.Users.FirstOrDefault(f => f.Email == factor.UserEmail);
                entity.Orders.Add(order);
                entity.SaveChanges();
            }
        }

        public static void EditOrder(Order order)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var currentOrder=entity.Orders.Attach(order);
                currentOrder.Amount = order.Amount;
                currentOrder.authority = order.authority;
                currentOrder.SubmitDateTime = order.SubmitDateTime;
                currentOrder.TransactionRefNumber = order.TransactionRefNumber;
                entity.SaveChanges();
            }
        }


        public static MyGridData GetAllOrders(int page, int rows, string sidx, string sord)
        {
            MyGridData myData = new MyGridData();
            myData.page = page;

            using (var entity = RepositoryUtil.OpenRepository())
            {
                var orders = entity.OrderDetails.OrderByDescending(x=>x.Order.SubmitDateTime).ToList();
                myData.total = (int)Math.Ceiling(orders.Count / (double)rows);
                myData.records = orders.Count;


                switch (sidx)
                {
                    case "FactorId":
                        if (sord == "asc")
                            orders.Sort((a, b) => a.FactorId.CompareTo(b.FactorId));
                        else
                            orders.Sort((a, b) => -1 * a.FactorId.CompareTo(b.FactorId));
                        break;
                    case "PaymentState":
                        if (sord == "asc")
                            orders.Sort((a, b) => String.Compare(a.Order.PaymentState, b.Order.PaymentState));
                        else
                            orders.Sort((a, b) => -1 * String.Compare(a.Order.PaymentState, b.Order.PaymentState));
                        break;
                    case "PaymentDateTime":
                        if (sord == "asc")
                            orders.Sort((a, b) => DateTime.Compare((DateTime)a.Order.PaymentDateTime, (DateTime)b.Order.PaymentDateTime));
                        else
                            orders.Sort((a, b) => -1 * DateTime.Compare((DateTime)a.Order.PaymentDateTime, (DateTime)b.Order.PaymentDateTime));
                        break;
                    case "FactorDateTime":
                        if (sord == "asc")
                            orders.Sort((a, b) => DateTime.Compare(a.Order.SubmitDateTime, b.Order.SubmitDateTime));
                        else
                            orders.Sort((a, b) => -1 * DateTime.Compare(a.Order.SubmitDateTime, b.Order.SubmitDateTime));
                        break;
                    case "ProductName":
                        if (sord == "asc")
                            orders.Sort((a, b) => string.Compare(a.Order.TransactionRefNumber, b.Order.TransactionRefNumber));
                        else
                            orders.Sort((a, b) => -1 * string.Compare(a.Order.TransactionRefNumber, b.Order.TransactionRefNumber));
                        break;
                    

                }
                
                
                orders = orders.GetRange((page - 1) * rows, Math.Min(orders.Count - ((page - 1) * rows), rows));
                var res = orders.Select(s => new { s.FactorId, s.Order.PaymentState, FactorDateTime=  DateTimeUtility.ConvertDatetimeToStringShmasi(s.Order.SubmitDateTime,'/'), PaymentDateTime = s.Order.PaymentDateTime!=null? DateTimeUtility.ConvertDatetimeToStringShmasi((DateTime)s.Order.PaymentDateTime,'/'):string.Empty, TransactionRefNumber=s.Order.TransactionRefNumber }).ToList();
                myData.rows = res;
                return myData;
            }
        }

        internal static List<Order> GetAllTransactionsByUser(string email)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var res=entity.Orders.Where(f => f.UserEmail == email).ToList();
                return res;
            }
        }

        public static List<Order> GetAllOrdersByDateInterval(DateTime low,DateTime high)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.Orders.Where(f=>f.SubmitDateTime>=low && f.SubmitDateTime<=high).ToList();
                foreach (var order in res)
                {
                    order.OrderDetails = order.OrderDetails.ToList();
                }
                return res;
            }
        }
        public static List<Order> GetAllOrdersByPriceInterval(long low, long high)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.Orders.Where(f => f.Amount >= low && f.Amount<= high).ToList();
                foreach (var order in res)
                {
                    order.OrderDetails = order.OrderDetails.ToList();
                }
                return res;
            }
        }
        public static List<Order> GetAllTransactions(DateTime low, DateTime high)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var res = entity.Orders.ToList();
                return res;
            }
        }

    }
}