﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class ArticleUtility
    {
        public static DBTransactionResult AddArticle(Article n)
        {

            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var timages = entity.uploadedImages.ToList();
                var ttt = n.ArticleImages.Select(s => s.ID).ToList();
                n.ArticleImages.Clear();
                foreach (var ti in timages)
                {
                    if (ttt.Contains(ti.ID))
                        n.ArticleImages.Add(new ArticleImage() { ImageLink = ti.ImageName });
                }
                n.Image = n.ArticleImages.FirstOrDefault().ImageLink;
                entity.Articles.Add(n);
                entity.SaveChanges();
                return DBTransactionResult.Successful;
                }
                catch (Exception ex)
                {
                    return DBTransactionResult.Error;
                }

            }
        }
        internal static List<Article> getTopArticles(int top)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var Articles = entity.Articles.OrderByDescending(n => n.InsertDateTime).ToList().GetRange(0, top > entity.Articles.ToList().Count ? entity.Articles.ToList().Count : top);
                foreach (var article in Articles)
                {
                    article.ArticleImages = article.ArticleImages.ToList();
                }
                return Articles;
            }
        }

        public static Article GetArticle(int ArticleCode)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var Article = entity.Articles.FirstOrDefault(n => n.ArticleCode == ArticleCode);
                return Article;
            }
        }

        internal static int GetArticleCount()
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.Articles.Count();
                return res;
            }
        }

        internal static List<Article> getArticlesIn(int low, int high)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var articleCount = entity.Articles.Count();
                var Articles = entity.Articles.ToList().GetRange(low, high<= articleCount?high:articleCount);
                foreach (var article in Articles)
                {
                    article.ArticleImages = article.ArticleImages.ToList();
                }
                return Articles;
            }
        }

        internal static MyGridData GetAllArticlesIn(int page, int rows, string sidx, string sord)
        {
            MyGridData myData = new MyGridData();
            myData.page = page;

            using (var entity = RepositoryUtil.OpenRepository())
            {
                var users = entity.Articles.ToList();
                myData.total = (int)Math.Ceiling(users.Count / (double)rows);
                myData.records = users.Count;
                switch (sidx)
                {
                    case "ArticleCode":
                        if (sord == "asc")
                            users.Sort((a, b) => a.ArticleCode.CompareTo(b.ArticleCode));
                        else
                            users.Sort((a, b) => -1 * a.ArticleCode.CompareTo(b.ArticleCode));
                        break;
                    case "ArticleTitle":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.ArticleTitle, b.ArticleTitle));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.ArticleTitle, b.ArticleTitle));
                        break;
                    case "Category":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.Category, b.Category));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.Category, b.Category));
                        break;

                }
                users = users.GetRange((page - 1) * rows, Math.Min(users.Count - ((page - 1) * rows), rows));
                var res = users.Select(s => new { s.ArticleCode, s.ArticleTitle, s.Category }).ToList();
                myData.rows = res;
                return myData;
            }
        }
    }
}