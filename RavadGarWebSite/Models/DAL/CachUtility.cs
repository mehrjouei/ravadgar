﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class CachUtility
    {
        public static Dictionary<int,List<Product>> GetHomePageLoadParts(int top)
        {
            var homeLoad = new Dictionary<int, List<Product>>();
            using (var entity=RepositoryUtil.OpenRepository())
            {
                //High Rated Prods
                var mostVisitedProducts = entity.Products.OrderByDescending(p => p.Visits).ToList();
                foreach (var p in mostVisitedProducts)
                {
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                }
                homeLoad[1] = mostVisitedProducts;
                //Last Prods
                var topNewProducts = entity.Products.OrderByDescending(p => p.AddDateTime).ToList().GetRange(0, top > entity.Products.ToList().Count ? entity.Products.ToList().Count : top);
                foreach (var p in topNewProducts)
                {
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductCombinations = p.ProductCombinations.ToList();
                }
                homeLoad[2] = topNewProducts;
                //top selled Prods
                var topSelledProducts = entity.Products.OrderByDescending(p => p.Price).ToList().GetRange(0, top > entity.Products.ToList().Count ? entity.Products.ToList().Count : top);
                foreach (var p in topSelledProducts)
                {
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductCombinations = p.ProductCombinations.ToList();
                }
                homeLoad[3] = topSelledProducts;
                //top Most Selled
                var topMostSelled = entity.Products.ToList().GetRange(0, top > entity.Products.ToList().Count ? entity.Products.ToList().Count : top).ToList();
                foreach (var p in topMostSelled)
                {
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductCombinations = p.ProductCombinations.ToList();
                }
                homeLoad[4] = topMostSelled;

                return homeLoad;
            }
        }
    }
}