﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class NewsUtility
    {
        public static DBTransactionResult AddNews(News n)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var timages = entity.uploadedImages.ToList();
                var ttt = n.NewsImages.Select(s => s.ID).ToList();
                n.NewsImages.Clear();
                foreach (var ti in timages)
                {
                    if (ttt.Contains(ti.ID))
                        n.NewsImages.Add(new NewsImage() { ImageLink = ti.ImageName });
                }
                n.Image = n.NewsImages.FirstOrDefault().ImageLink;
                entity.News.Add(n);
                entity.SaveChanges();
                return DBTransactionResult.Successful;
            }
        }


        internal static List<News> getTopNews(int top)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var newsCount = entity.News.Count();
                top = top > newsCount ? newsCount : top;
                var res = entity.News.OrderByDescending(n => n.InsertDateTime).ToList().GetRange(0, top);
                return res;
            }
        }

        public static News GetNews(int newsCode)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var news = entity.News.FirstOrDefault(n => n.NewsCode == newsCode);
                return news;
            }
        }
        internal static int GetNewsCount()
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.News.Count();
                return res;
            }
        }

        internal static List<News> getNewsIn(int low, int high)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var newCount = entity.News.Count();
                var Articles = entity.News.ToList().GetRange(low, high <= newCount ? high : newCount);
                foreach (var article in Articles)
                {
                    article.NewsImages = article.NewsImages.ToList();
                }
                return Articles;
            }
        }
        internal static List<News> getAllNews()
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var Articles = entity.News.ToList();
                foreach (var article in Articles)
                {
                    article.NewsImages = article.NewsImages.ToList();
                }
                return Articles;
            }
        }
        internal static MyGridData GetAllNewsIn(int page, int rows, string sidx, string sord)
        {
            MyGridData myData = new MyGridData();
            myData.page = page;

            using (var entity = RepositoryUtil.OpenRepository())
            {
                var users = entity.News.ToList();
                myData.total = (int)Math.Ceiling(users.Count / (double)rows);
                myData.records = users.Count;
                switch (sidx)
                {
                    case "NewsCode":
                        if (sord == "asc")
                            users.Sort((a, b) => a.NewsCode.CompareTo(b.NewsCode));
                        else
                            users.Sort((a, b) => -1 * a.NewsCode.CompareTo(b.NewsCode));
                        break;
                    case "NewsTitle":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.NewsTitle, b.NewsTitle));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.NewsTitle, b.NewsTitle));
                        break;
                    case "Category":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.Category, b.Category));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.Category, b.Category));
                        break;

                }
                users = users.GetRange((page - 1) * rows, Math.Min(users.Count - ((page - 1) * rows), rows));
                var res = users.Select(s => new { s.NewsCode, s.NewsTitle, s.Category }).ToList();
                myData.rows = res;
                return myData;
            }
        }
    }
}