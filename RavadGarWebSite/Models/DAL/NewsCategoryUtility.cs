﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class NewsCategoryUtility
    {
    
        public static List<NewsCategory> GetAllCategories()
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var res = entity.NewsCategories.ToList().OrderBy(c => c.CategoryID).ToList();
                return res;
            }
        }

    }
}