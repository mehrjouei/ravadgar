﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;


namespace RavadGarWebSite.Models.DAL
{
    public static class ProductUtility
    {
        public static DBTransactionResult AddProduct(Product p)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var timages = entity.uploadedImages.ToList();
                    var ttt = p.ProductImages.Select(s => s.ID).ToList();
                    p.ProductImages.Clear();
                    foreach (var ti in timages)
                    {
                        if (ttt.Contains(ti.ID))
                            p.ProductImages.Add(new ProductImage() { ImageLink = ti.ImageName });
                    }
                    if (p.ProductImages.Count > 0)
                        p.Image = p.ProductImages.FirstOrDefault().ImageLink;
                    entity.Products.Add(p);
                    entity.SaveChanges();
                    return DBTransactionResult.Successful;
                }
                catch (Exception ex)
                {
                    return DBTransactionResult.Error;
                }

            }
        }

        internal static Dictionary<OrderViewModel, Product> GetProductDetailsById(List<OrderViewModel> list)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var pids = list.Select(s => s.ProductId).ToList();
                var productDetails = entity.Products.Where(f => pids.Contains(f.PCode)).ToList();
                foreach (var com in productDetails)
                {
                    com.ProductCombinations = com.ProductCombinations.ToList();
                }
                var prodDic = productDetails.ToDictionary(f => list.FirstOrDefault(t => t.ProductId == f.PCode), f => f);
                return prodDic;
            }
        }

        public static List<Product> GetProductByKeyword(string keyword)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.Products.Where(p => p.PFName.Contains(keyword) || p.PEName.Contains(keyword)).ToList();
                return res;
            }
        }

        internal static int GetProductCombinationCount(long? combinationId)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var productCombination = entity.ProductCombinations.FirstOrDefault(f => f.Id == (int)combinationId);
                return (int)productCombination.CombinationNumber;
            }
        }

        internal static int GetProductNumber(int productId)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var product = entity.Products.FirstOrDefault(f => f.PCode == (int)productId);
                return (int)product.Number;
            }
        }

        public static List<Product> GetProductByCategoryTitle(string catTitle)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.Products.ToList().Where(p => p.ProductCategories.ToList().Where(f => f.Category.CategoryName.Contains(catTitle)).ToList().Count > 0).ToList();
                foreach (var p in res)
                {
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductImages = p.ProductImages.ToList();
                }
                return res;
            }
        }

        public static DBTransactionResult UpdateProduct(Product p)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                Product prod = entity.Products.FirstOrDefault(f => f.PCode == p.PCode);
                prod.Active = p.Active;
                prod.Brand = p.Brand;
                prod.Description = p.Description;
                prod.FullDescription = p.FullDescription;
                prod.Image = p.Image;
                prod.Number = p.Number;
                prod.PEName = p.PEName;
                prod.PFName = p.PFName;
                prod.Price = p.Price;
                prod.Gender = p.Gender;

                entity.ProductCategories.RemoveRange(entity.ProductCategories.Where(f => f.ProductId == p.PCode));
                for (int i = 0; i < p.ProductCategories.Count; i++)
                {
                    (p.ProductCategories.ToList()[i] as ProductCategory).ProductId = p.PCode;
                    prod.ProductCategories.Add(p.ProductCategories.ToList()[i]);
                }


                entity.ProductFeatures.RemoveRange(entity.ProductFeatures.Where(f => f.ProductCode == p.PCode));
                for (int i = 0; i < p.ProductFeatures.Count; i++)
                {
                    (p.ProductFeatures.ToList()[i] as ProductFeature).ProductCode = p.PCode;
                    prod.ProductFeatures.Add(p.ProductFeatures.ToList()[i]);
                }

                entity.ProductCombinations.RemoveRange(entity.ProductCombinations.Where(f => f.PId == p.PCode));
                for (int i = 0; i < p.ProductCombinations.Count; i++)
                {
                    (p.ProductCombinations.ToList()[i] as ProductCombination).PId = p.PCode;
                    prod.ProductCombinations.Add(p.ProductCombinations.ToList()[i]);
                }

                var timages = entity.uploadedImages.ToList();
                foreach (var i in p.ProductImages)
                {
                    if (prod.ProductImages.FirstOrDefault(f => f.ID == i.ID) == null)
                    {
                        i.ImageLink = timages.FirstOrDefault(f => f.ID == i.ID).ImageName;
                        prod.ProductImages.Add(i);

                    }
                }


                prod.Image = prod.ProductImages.FirstOrDefault() != null ? prod.ProductImages.FirstOrDefault().ImageLink : null;
                prod.UserScore = p.UserScore;
                prod.Visits = p.Visits;

                var res = entity.SaveChanges();
                if (res > 0)
                    return DBTransactionResult.Successful;

            }
            return DBTransactionResult.Error;
        }

        public static List<ProductImage> GetImages(int pCode)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.Products.FirstOrDefault(f => f.PCode == pCode).ProductImages.ToList();
                return res;
            }
        }

        internal static DBTransactionResult DeleteProduct(int pCode)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var p = entity.Products.FirstOrDefault(f => f.PCode == pCode);
                    entity.Products.Remove(p);
                    var res = entity.SaveChanges();
                    if (res > 0)
                        return DBTransactionResult.Successful;
                }
                catch { }
            }
            return DBTransactionResult.Error;
        }

        public static List<Product> GetProductsByCategoryId(int id)
        {
            //findChildCategories(id);
            List<Product> cats = new List<Product>();
            using (var entity = RepositoryUtil.OpenRepository())
            {
                //foreach (var c in catIds)
                //{
                var ps = entity.Products.ToList().Where(f => f.ProductCategories.ToList().Select(s => s.CategoryId).ToList().Contains(id)).ToList();
                cats.AddRange(ps);
                //}
                foreach (var p in cats)
                {
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                }
                return cats.Distinct().ToList();
            }
        }

        private static void findChildCategories(int id)
        {
            List<Category> cats = null;
            using (var entity = RepositoryUtil.OpenRepository())
            {
                cats = entity.Categories.ToList();
            }
            MakeTree(cats, id);
            catIds = catIds.Distinct().ToList();
        }
        static List<int> catIds = new List<int>();
        private static void MakeTree(List<Category> cats, int id)
        {
            catIds.Add(id);
            var childs = cats.Where(c => c.ParentCategoryID == id).ToList();
            foreach (var c in childs)
            {
                MakeTree(cats, c.CategoryID);
            }
        }

        public static List<Product> GetMostVisitedProducts()
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var highRateProducts = entity.Products.OrderByDescending(p => p.Visits).ToList();
                foreach (var p in highRateProducts)
                {
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                }
                return highRateProducts;
            }
        }

        public static string getImageName(int pId, int imageId)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var image = entity.ProductImages.FirstOrDefault(i => i.Pcode == pId && i.ID == imageId);
                if (image != null)
                    return image.ImageLink;
                else
                    return string.Empty;
            }
        }
        public static DBTransactionResult RemoveImage(int pId, int imageid)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var image = entity.ProductImages.FirstOrDefault(i => i.ID == imageid && i.Pcode == pId);
                    entity.ProductImages.Remove(image);
                    var res = entity.SaveChanges();
                    if (res > 0)
                        return DBTransactionResult.Successful;
                    else
                        return DBTransactionResult.Error;
                }
                catch
                {
                    return DBTransactionResult.DataBaseError;
                }
            }
        }

        public static List<Product> GetTopNewProducts(int top)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var products = entity.Products.OrderByDescending(p => p.AddDateTime).ToList().GetRange(0, top > entity.Products.ToList().Count ? entity.Products.ToList().Count : top);
                foreach (var p in products)
                {
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductCombinations = p.ProductCombinations.ToList();
                }
                return products;
            }
        }
        public static List<Product> GetTopsellerProducts(int top)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var products = entity.Products.OrderByDescending(p => p.Price).ToList().GetRange(0, top > entity.Products.ToList().Count ? entity.Products.ToList().Count : top);
                foreach (var p in products)
                {
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductCombinations = p.ProductCombinations.ToList();
                }
                return products;
            }
        }

        public static List<Product> GetTopMostSelled(int top)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.Products.ToList().GetRange(0, top > entity.Products.ToList().Count ? entity.Products.ToList().Count : top).ToList();
                foreach (var p in res)
                {
                    p.ProductImages = p.ProductImages.ToList();
                    p.ProductFeatures = p.ProductFeatures.ToList();
                    p.ProductCategories = p.ProductCategories.ToList();
                    p.ProductCombinations = p.ProductCombinations.ToList();
                }
                return res;
            }
        }

        public static Product GetProduct(int pCode)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var prod = entity.Products.FirstOrDefault(p => p.PCode == pCode);
                    if (prod != null)
                    {
                        prod.ProductImages = prod.ProductImages.ToList();
                        prod.ProductFeatures = prod.ProductFeatures.ToList();
                        prod.ProductCategories = prod.ProductCategories.ToList();
                        prod.ProductCombinations = prod.ProductCombinations.ToList();
                    }
                    return prod;
                }
                catch { }
            }
            return new Product();
        }
        public static List<Product> GegtProducts()
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var products = entity.Products.ToList();
                return products;
            }
        }
        internal static MyGridData GetAllProductsIn(int page, int rows, string sidx, string sord)
        {
            MyGridData myData = new MyGridData();
            myData.page = page;

            using (var entity = RepositoryUtil.OpenRepository())
            {
                var users = entity.Products.ToList();
                myData.total = (int)Math.Ceiling(users.Count / (double)rows);
                myData.records = users.Count;
                switch (sidx)
                {
                    case "PCode":
                        if (sord == "asc")
                            users.Sort((a, b) => a.PCode.CompareTo(b.PCode));
                        else
                            users.Sort((a, b) => -1 * a.PCode.CompareTo(b.PCode));
                        break;
                    case "PFName":
                        if (sord == "asc")
                            users.Sort((a, b) => String.Compare(a.PFName, b.PFName));
                        else
                            users.Sort((a, b) => -1 * String.Compare(a.PFName, b.PFName));
                        break;
                    case "Price":
                        if (sord == "asc")
                            users.Sort((a, b) => a.Price.Value.CompareTo(b.Price.Value));
                        else
                            users.Sort((a, b) => -1 * a.Price.Value.CompareTo(b.Price.Value));
                        break;
                    case "Number":
                        if (sord == "asc")
                            users.Sort((a, b) => a.Number.Value.CompareTo(b.Number.Value));
                        else
                            users.Sort((a, b) => -1 * a.Number.Value.CompareTo(b.Number.Value));
                        break;
                        //case "Category":
                        //    if (sord == "asc")
                        //        users.Sort((a, b) => a.Category.HasValue.CompareTo(b.Category.HasValue));
                        //    else
                        //        users.Sort((a, b) => -1 * a.Category.HasValue.CompareTo(b.Category.HasValue));
                        //    break;
                }
                users = users.GetRange((page - 1) * rows, Math.Min(users.Count - ((page - 1) * rows), rows));
                var res = users.Select(s => new { s.PCode, s.PFName, s.Price, s.Number, Category = 1 }).ToList();
                myData.rows = res;
                return myData;
            }
        }

        /// <summary>
        /// Warning:: This function needs refinment and is not reliable
        /// </summary>
        /// <param name="prodId"></param>
        /// <param name="cId"></param>
        /// <returns></returns>
        public static DBTransactionResult RemoveProductCategory(int prodId, int cId)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    var pcat = entity.ProductCategories.FirstOrDefault(c => c.CategoryId == cId && c.ProductId == prodId);
                    entity.ProductCategories.Remove(pcat);
                    var res = entity.SaveChanges();
                    if (res > 0)
                        return DBTransactionResult.Successful;
                }
                catch { }
            }
            return DBTransactionResult.Error;
        }

        internal static int GetTempImageID(string rndName)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                var res = entity.uploadedImages.FirstOrDefault(f => f.ImageName == rndName);
                if (res != null)
                    return res.ID;
                else return -1;
            }
        }

        /// <summary>
        /// Warning:: This function needs refinment and is not reliable
        /// </summary>
        /// <param name="prodId"></param>
        /// <param name="cId"></param>
        /// <returns></returns>
        public static DBTransactionResult AddProductCategory(int prodId, int cId)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    entity.ProductCategories.Add(new ProductCategory()
                    {
                        CategoryId = cId,
                        ProductId = prodId
                    });
                    var res = entity.SaveChanges();
                    if (res > 0)
                        return DBTransactionResult.Successful;
                }
                catch { }
            }
            return DBTransactionResult.Error;
        }

        public static DBTransactionResult AddImagetoTemp(uploadedImage up)
        {
            using (var entity = RepositoryUtil.OpenRepository())
            {
                try
                {
                    entity.uploadedImages.Add(up);
                    var res = entity.SaveChanges();
                    if (res > 0)
                        return DBTransactionResult.Successful;
                }
                catch { }
            }
            return DBTransactionResult.Error;
        }
    }
}