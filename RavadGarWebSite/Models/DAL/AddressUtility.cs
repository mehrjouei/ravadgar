﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Models.DAL
{
    public static class AddressUtility
    {
        public static void AddAddress(Address address)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                entity.Addresses.Add(address);
                entity.SaveChanges();
            }
        }
        public static Address GetAddressByUser(string email)
        {
            var e = email;
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var address=entity.Addresses.FirstOrDefault(f => f.Customer == email);
                return address;
            }
        }

        internal static void UpdateAddress(Address address)
        {
            using (var entity=RepositoryUtil.OpenRepository())
            {
                var add=entity.Addresses.FirstOrDefault(f=>f.Customer== address.Customer);
                add.Address1 = address.Address1;
                add.City = address.City;
                add.State = address.State;
                add.PostalCode = address.PostalCode;
                add.Tel = address.Tel;
                add.Mobile = address.Mobile;
                entity.SaveChanges();
            }
        }
    }
}