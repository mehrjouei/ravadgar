﻿using RavadGarWebSite.Framwork.BaseClasses.Enums;
using RavadGarWebSite.Framwork.BaseClasses.Utilities;
using RavadGarWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RavadGarWebSite.Framwork.Attributes
{

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PermissionAttribute : ActionFilterAttribute
    {
        private readonly Permissions required;
        public PermissionAttribute(Permissions required)
        {
            this.required = required;
        }


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            switch (this.required)
            {
                case Permissions.user:
                    var user = filterContext.HttpContext.Session[SessionKeys.user] as Models.User;
                    if (user == null)
                    {
                        filterContext.HttpContext.Session.Add(SessionKeys.ComeBackURL, filterContext.HttpContext.Request.Url.PathAndQuery);
                        var url = new UrlHelper(filterContext.RequestContext);
                        var loginUrl = url.Content("~/Login");//Error Page Url
                        filterContext.HttpContext.Response.Redirect(loginUrl, true);
                    }
                    break;
                case Permissions.admin:
                    var admin = filterContext.HttpContext.Session[SessionKeys.admin] as Admin;
                    if (admin == null)
                    {
                        var url = new UrlHelper(filterContext.RequestContext);
                        var loginUrl = url.Content("~/Login/Index");//Error Page Url
                        filterContext.HttpContext.Response.Redirect(loginUrl, true);
                    }
                    break;
                default:
                    break;
            }
        }



    }



    [AttributeUsage(AttributeTargets.Method| AttributeTargets.Class, AllowMultiple = false)]
    public class MyAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            var user = httpContext.Session[SessionKeys.user] as Models.User;
            if (user == null)
                return false;
            else
                return true;

        }
        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    if (!AuthorizeCore(filterContext.HttpContext))
        //    {
        //        HandleUnauthorizedRequest(filterContext);

        //    }
        //}
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = "Login",
                                action = "Login"
                            })
                        );

        }

    }
}
