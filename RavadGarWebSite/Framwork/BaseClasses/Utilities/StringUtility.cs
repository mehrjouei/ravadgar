﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framwork.BaseClasses.Utilities
{
    public static class StringUtility
    {
        public static string GenerateRndString()
        {
            var gid = Guid.NewGuid();
            var hash = Math.Abs(gid.GetHashCode()).ToString();
            var rndString = hash + DateTime.Now.ToString("yyyyMMddHHmmss");
            return rndString;
        }
        public static string GenerateSimpleRndString()
        {
            var gid = Guid.NewGuid();
            var rndString = Math.Abs(gid.GetHashCode()).ToString();
            return rndString;
        }
    }
}