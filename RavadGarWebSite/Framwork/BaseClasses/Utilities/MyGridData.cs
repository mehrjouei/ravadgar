﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framwork.BaseClasses.Utilities
{
    public struct MyGridData
    {
        public int page;
        public int records;
        public object rows;
        public int total;
    }
}