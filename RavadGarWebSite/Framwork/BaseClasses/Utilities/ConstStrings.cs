﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framwork.BaseClasses.Utilities
{
    public static class SessionKeys
    {
        public static string user = "user";
        public static string factor = "factor";
        public static string admin = "admin";
        public static string ComeBackURL = "ComeBackURL";
    }
    public static class ImagePathes
    {
        public static string ParentPath = "productimages";
        public static string ThumbNail = "ThumbNail";
        public static string Images = "Images";
    }
}