﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framwork.BaseClasses.Utilities
{
    public static class WebUtility
    {
        public static T GetClone<T>(T data)
        {
            var res=JsonConvert.SerializeObject(data);
            var x = JsonConvert.DeserializeObject<T>(res);
            return x;
        }
    }
}