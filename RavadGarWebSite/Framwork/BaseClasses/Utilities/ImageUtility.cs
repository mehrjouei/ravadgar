﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeCarvings.Piczard;
using System.IO;
using System.Diagnostics;

namespace RavadGarWebSite.Framwork.BaseClasses.Utilities
{
    public static class ImageUtility
    {
        public static void RemoveImage(string path)
        {
            try { }
            finally
            {
                try
                {
                    File.Delete(path);
                }
                catch { }
                Stopwatch sw = new Stopwatch();
                sw.Start();
                bool isDeleted = false;
                while (sw.ElapsedMilliseconds < 1000 && !isDeleted)
                    try { File.Delete(path); }
                    catch { }
                sw.Stop();
            }
        }
    }
}