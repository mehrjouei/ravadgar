﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framwork.BaseClasses.Utilities
{
    public static class AccountingUtility
    {
        public static string GetSecurePassword(string password)
        {
            return GetHashCode(password);
        }
        private static string GetHashCode(string password)
        {
            return password.GetHashCode().ToString();
        }
    }
}