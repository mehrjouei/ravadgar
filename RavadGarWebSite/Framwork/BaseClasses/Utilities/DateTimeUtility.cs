﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framwork.BaseClasses.Utilities
{
    public static class DateTimeUtility
    {
        public static string ConvertDatetimeToStringShmasi(DateTime datetime, char seperator = ' ')
        {
            string res = "";
            string sep = seperator == ' ' ? "" : seperator.ToString();
            System.Globalization.PersianCalendar shamsi = new System.Globalization.PersianCalendar();

            int shamsiYear = shamsi.GetYear(datetime);
            int shamsiMonth = shamsi.GetMonth(datetime);
            int shamsiDay = shamsi.GetDayOfMonth(datetime);

            string sY = shamsiYear.ToString();
            string sM = shamsiMonth < 10 ? "0" + shamsiMonth.ToString() : shamsiMonth.ToString();
            string sD = shamsiDay < 10 ? "0" + shamsiDay.ToString() : shamsiDay.ToString();
            res = $"{sY}{sep}{sM}{sep}{sD} {datetime.Hour}:{datetime.Minute}:{datetime.Second}";

            return res;
        }
    }
}