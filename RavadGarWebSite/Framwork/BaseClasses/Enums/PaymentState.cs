﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framework.BaseClasses.Enum
{
    public enum PaymentState
    {
        Payed,
        NotPayed,
        Pending,
        Failed
    }
}