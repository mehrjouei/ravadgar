﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavadGarWebSite.Framwork.BaseClasses.Enums
{
    public enum Permissions
    {
        user,
        admin
    }
    public enum DBTransactionResult
    {
        Successful,
        Error,
        DataBaseError
    }
}