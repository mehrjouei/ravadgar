﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RavadGarWebSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
            name: "ProductFrameShow",
            url: "product/frameView/{id}",
            defaults: new { controller = "Product", action = "frameView", id = UrlParameter.Optional}
            );
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
            name: "ProductShow",
            url: "product/{id}/{keyword}",
            defaults: new { controller = "Product", action = "Index", id = UrlParameter.Optional, keyword = "" }
            );
            routes.MapRoute(
           name: "ArticleShowAll",
           url: "Article/List/{id}",
           defaults: new { controller = "Article", action = "List", id = UrlParameter.Optional }
           );
            routes.MapRoute(
            name: "ArticleShow",
            url: "Article/{id}",
            defaults: new { controller = "Article", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
name: "NewsAll",
url: "news/All",
defaults: new { controller = "News", action = "All", id = UrlParameter.Optional }
);
            routes.MapRoute(
name: "NewsShow",
url: "news/{id}",
defaults: new { controller = "News", action = "Index", id = UrlParameter.Optional }
);
            routes.MapRoute(
name: "PagesShow",
url: "Page/{Name}",
defaults: new { controller = "Page", action = "Index", Name = UrlParameter.Optional }
);
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
            name: "searchShow",
            url: "search/{id}/{catTitle}",
            defaults: new { controller = "Search", action = "Index", id = UrlParameter.Optional, catTitle = "" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );



        }
    }
}
