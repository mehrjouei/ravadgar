var validationresult = true;
var errorarray = [];
var messages = {
    "waiting": "لطفا شکیبا باشید...",
    "required": "این فیلد نمیتواند خالی باشد",
    "email": "این فیلد باید ایمیل باشد",
    "digit": "این فیلد فقط عدد میباشد",
    "max": "این فیلدنمیتواند بزرگتر از {0} باشد",
    "checkMail":"این ایمیل قبلا ثبت شده است",
    "between": "این فیلد باید بین {0} و {1} باشد",
    "characteri": "این فیلد فقط حروف میباشد",
    "repeatEqual": " {0} برابر مقدار اصلی نیست",
    "noRowSelected": "ابتدا باید یکی از سطرها را انتخاب کنید.",
    "excelExport": "خروجی اکسل",
    "pdfExport": "خروجی pdf",
    "advancedSearch": "جستجوی پیشرفته",
    "reloadGrid": "بروز رسانی جدول",
    "nextPage": "صفحه بعد",
    "previousPage": "صفحه قبل",
    "firstPage": "صفحه اول",
    "lastPage": "صفحه آخر",
    "rowsPerPage": "تعداد رکوردها در هر صفحه",
    "pagerToolbarButton": "نوار ابزار",
    "formBtnCancel": "انصراف",
    "formBtnSearch": "جستجو",
    "search": "جستجو",
    "gridAdvancedSearchOR": "شرط ها با یکدیگر OR شوند.",
    "gridAdvancedSearchAND": "شرط ها با یکدیگر AND شوند.",
    "gridAdvancedSearchAddRule": "افزودن شرط",
    "gridAdvancedSearchRemoveRule": "حذف شرط",
    "gridAdvancedSearchOperation_eq": "برابر با",
    "gridAdvancedSearchOperation_ne": "نابرابر با",
    "gridAdvancedSearchOperation_lt": "کوچکتر از",
    "gridAdvancedSearchOperation_le": "کوچکتر یا مساوی",
    "gridAdvancedSearchOperation_gt": "بزرگتر از",
    "gridAdvancedSearchOperation_ge": "بزرگتر یا مساوی",
    "gridAdvancedSearchOperation_bw": "شروع شود با",
    "gridAdvancedSearchOperation_bn": "شروع نشود با",
    "gridAdvancedSearchOperation_in": "باشد عضو ",
    "gridAdvancedSearchOperation_ni": "نباشد عضو",
    "gridAdvancedSearchOperation_ew": "تمام شود با",
    "gridAdvancedSearchOperation_en": "تمام نشود با",
    "gridAdvancedSearchOperation_cn": "باشد حاوی",
    "gridAdvancedSearchOperation_nc": "نباشد حاوی",
    "gridAdvancedSearchOperation_nu": "خالی باشد",
    "gridAdvancedSearchOperation_nn": "خالی نباشد",
    "validationTitle": "لطفا خطاهای زیر را بررسی کنید:",
    "memberNotFoundNationalCode": "کد ملی یافت نشد"
};
var isRtl = true;
Array.prototype.remove = function (v) {
    this.splice(this.indexOf(v) === -1 ? this.length : this.indexOf(v), 1);
};
function validateitem(value) {
    var tem = 0;
    var valstr = $(value).attr('data-mgwvalidator');
    if (valstr == null) {
        return;
    }
    var roles = valstr.split(",");
    var i;

    for (i = 0; i < roles.length; i++) {
        var min = roles[i].indexOf("[");
        var max2 = roles[i].indexOf("]");
        var data = roles[i].substring(min + 1, max2);
        if (data !== "")
        {
            var fname = roles[i].substring(0, min);
            var a = fname + "-[" + data + "]," + $(value).attr('id');


            if (typeof window[fname] === "function") {

                errorarray.remove(fname + "-" + data + "," + $(value).attr('id'));
                if (!eval(fname)(value, data))
                {
                    tem = 1;

                }

                if (tem === 0) {
                    setok($(value));
                }
                if (!validationresult) {

                    removealertofitem(value);
                    seterrors();
                }
            }
        }
        else if (typeof window[roles[i]] === "function") {
            errorarray.remove(roles[i] + "," + $(value).attr('id'));
            if (!eval(roles[i])(value, data))
            {
                tem = 1;
            }
            if (tem === 0) {
                setok($(value));
            }
            if (!validationresult) {

                removealertofitem(value);
                seterrors();
            }
        }
    }

}
$.fn.validation = function () {
    removealerts();
    errorarray.length = 0;
    validationresult = true;
    $(this).find('input').each(function () {
        validateitem(this);
    });
    if (!validationresult) {
//        seterrors();
        return false;
    }
    else {
        return true;
    }
};
function validateOnBlur(id){
     $(id).find('input').on("blur", function () {
        if (!$(this).hasClass("disabled"))
            validateitem(this);
    });
}
function setok(val) {
    $(val).addClass("mgw-validator-ok");
    $(val).next('.mgw-validator-alert').remove();
}
function removealerts() {
    $(".mgw-validator-alert").remove();
    $(".mgw-validator-error").removeClass("mgw-validator-error");
}
function removealertofitem(val) {
    $(val).next(".mgw-validator-alert").remove();
    $(val).removeClass("mgw-validator-error");
}
function required(value, data) {
    if ($(value).val() === "") {
        seterror("required", value);
        validationresult = false;
        return false;
    }
    else {
        return true;
    }
}
function email(value, data) {
    if ($(value).val().split("@").length != 2 || $(value).val().split(".").length<2) {
        seterror("email", value);
        validationresult = false;
        return false;
    }
    else {
        return true;
    }
}
function max(value, data) {
    var max = data;
    if (parseInt($(value).val(), 10) > parseInt(max, 10)) {
        seterror("max-" + data, value);
        validationresult = false;
        return false;
    }
    return true;
}
function checkMail(value,data) {
    var res = funjs_sendObjectToServer("/home/ExistEmail", { "email": $(value).val() });
    console.log(res.result);
    if (res.result != "False") {
        seterror("checkMail", value);
        validationresult = false;
        return false;
    }
    else {
        
        return false;
    }
}
function repeatEqual(value, data) {
    if ($(value).val() !== $("#"+data).val()) {
        seterror("repeatEqual-"+data, value);
        validationresult = false;
        return false;
    }
    return true;
}
function between(value, data) {
    var middle = data.indexOf("-");
    lindex = data.substring(0, middle);
    mindex = data.substring(middle + 1, data.lenght);
    if (parseInt($(value).val(), 10) < parseInt(lindex, 10) || parseInt($(value).val(), 10) > parseInt(mindex, 10)) {
        seterror("between-" + data, value);
        validationresult = false;
        return false;
    }
    return true;
}
function digit(value, data) {
    validChar = '1234567890';
    strlen = $(value).val().length;
    for (i = 0; i < strlen; i++) {
        if (validChar.indexOf($(value).val().charAt(i)) >= 0) {
        }
        else {
            seterror("digit", value);
            validationresult = false;
            return false;
        }
    }
    return true;
}
function characteri(value, data) {

    notvalidChar = '1234567890=-!@#$%^&*()_+\|}{;:?/.><,"\'';
    strlen = $(value).val().length;
    val = $(value).val().toUpperCase();
    for (i = 0; i < strlen; i++) {
        if (notvalidChar.indexOf($(value).val().charAt(i)) < 0) {
        }
        else {
            seterror("characteri", value);
            validationresult = false;
            return false;
        }
    }
    return true;
}
function seterror(str, item) {
    if ($.inArray(str + "," + $(item).attr('id')) < 0) {
        errorarray.push(str + "," + $(item).attr('id'));
    }
}

function seterrors() {
    removealerts();
    var temp = "";
    var messa = "";
    for (i = 0; i < errorarray.length; ++i) {
        temp = errorarray[i];
        var error = temp.split(",");
        var errorPart = error[0].split("-");
        if (errorPart.length === 2) {
            messa = messages[errorPart[0]].replace("{0}", errorPart[1]);
        }
        else if (errorPart.length === 3) {
            messa = messages[errorPart[0]].replace("{0}", errorPart[1]).replace("{1}", errorPart[2]);
        }
        else {
            messa = messages[error[0]];
        }
        $("#" + error[1]).removeClass("mgw-validator-ok");
        $("#" + error[1]).addClass("mgw-validator-error");
        $("<div class='" + ((isRtl) ? "alert" : "alert-ltr") + " alert-danger mgw-validator-alert'><a class='close' data-dismiss='alert'>×</a>" + messa + " </div>").insertAfter("#" + error[1]);
    }
}


	