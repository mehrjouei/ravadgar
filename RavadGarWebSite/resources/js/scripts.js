﻿function calcWidth() {
    rollwid = $(".rollProductsBodyDiv").width();
    var liSize = $(".rollProductsBodyDiv ul li").width();
    var numberofli = (parseInt(rollwid / liSize));
    var marginli = (rollwid - (numberofli * liSize)) / numberofli;
    $(".product_list").each(function () {
        $(this).find("li").each(function () {
            $(this).css("margin-right", marginli / 2);
            $(this).css("margin-left", marginli / 2);
        });
    });
    $(".rollProductsBodyDiv").each(function () {
        var ulwidth = 0;
        $(this).find("li").each(function () {
            ulwidth += $(this).width() + marginli;
        });
        $(this).find("ul").css("width", ulwidth/2 + 10);
    });
    var rollwidth = $(".rollProductsBodyDiv").width();
}
$(document).ready(function () {
    calcWidth();
    $(".rightarr").click(function () {
        var leftspace = $(this).parent().parent().next(".rollProductsBodyDiv").children(".rollProductsScrollDiv").scrollLeft();
        $(this).parent().parent().next(".rollProductsBodyDiv").children(".rollProductsScrollDiv").animate({
            'scrollLeft': leftspace + rollwid
        }, { duration: 700, queue: false });
    });
    $(".leftarr").click(function () {
        var leftspace = $(this).parent().parent().next(".rollProductsBodyDiv").children(".rollProductsScrollDiv").scrollLeft();
        $(this).parent().parent().next(".rollProductsBodyDiv").children(".rollProductsScrollDiv").animate({
            'scrollLeft': leftspace - rollwid
        }, { duration: 700, queue: false });
    });
    $(".rightarr").trigger("click");
})

function funjs_sendObjectToServer(url, object) {
    var returnvalue = "";
    $.ajax({
        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },
        type: "POST",
        url: url,
        data: JSON.stringify(object),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function (xhr) {
        },
        success: function (data) {
            returnvalue = data;
        },
        error: function (data) {
            returnvalue = "ajaxError";
        },
        failure: function (errMsg) {
            returnvalue = "ajaxFailure";
        }
    });
    return returnvalue;
}

function funjs_sendObjectToServerAsync(url, object, sFunc) {
    var returnvalue = "";
    $.ajax({
        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },
        type: "POST",
        url: url,
        data: JSON.stringify(object),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        beforeSend: function (xhr) {
        },
        success: function (data) {
            sFunc(data);
        },
        error: function (data) {
            returnvalue = "ajaxError";
        },
        failure: function (errMsg) {
            returnvalue = "ajaxFailure";
        }
    });
    return returnvalue;
}


function showLoadingOn(place) {
    $(place).css("position", "relative");
    $(place).append("<div class='loadingDiv'><img src='/resources/images/userpanel/loading.gif' /></div>");
}
function hideLoadingOn(place) {
    $(place + " .loadingDiv").remove();
}

function funjs_getFormObject(formid) {
    var prefix = $("#" + formid).attr("data-prefix");
    var obj = {};
    var fieldName = "";
    var fieldType = "";
    var fieldValue = "";
    var dataType = "";
    $("#" + formid).find("input, textarea").each(function (index) {
        fieldName = $(this).attr("name");
        fieldType = $(this).attr("type");
        fieldValue = $(this).val();
        if (fieldType == "checkbox") {
            if ($(this).is(':checked')) {
                obj[fieldName] += fieldValue;
            }
        }
        else {
            dataType = $(this).attr("data-type");
            if (dataType == "int") {
                obj[fieldName] = parseInt(fieldValue);

            }
            else if (dataType == "float") {
                //console.log("float");
                obj[fieldName] = parseFloat(fieldValue);
            }
            else if (dataType == "dateTime") {
                obj[fieldName] = fieldValue + ":00";
            }
            else {
                obj[fieldName] = fieldValue;
            }
        }

    });
    $("#" + formid).find("select").each(function (index) {
        fieldName = $(this).attr("name");
        //fieldName = fieldName.slice(prefix.length + 1).slice(0, -1);
        fieldType = $(this).attr("type");
        fieldValue = $(this).val();
        dataType = $(this).attr("data-type");
        if (dataType == "int") {
            obj[fieldName] = parseInt(fieldValue);
        }
        else {
            if (fieldType == "date") {
                obj[fieldName] = new Date(fieldValue);
            } else {
                obj[fieldName] = fieldValue;
            }
        }

    });
    return obj;
}
var searchRsultShowFlag = 0;
function showMessage(messageValue) {
    $("#messageBody").html(messageValue);
    $('#messageModal').modal({
        show: 'true'
    });
}

// search text box
$(document).ready(function () {

    $("#searchTextBox").keyup(function () {
        if ($(this).val().length > 2) {
            $("#searchResultListDropDown").show();
            funjs_sendObjectToServerAsync("/home/searchFilter", { keyword: $(this).val() }, showSearchFilter);
            searchRsultShowFlag = 0;
        }
    });
    $("#searchTextBox").blur(function () {
        searchRsultShowFlag++;
        hideResultBox();
    });
    $("#searchResultListDropDown").mouseenter(function () {
        searchRsultShowFlag--;
    });
    $("#searchResultListDropDown").mouseleave(function () {
        searchRsultShowFlag++;
        hideResultBox();
    });
})

function hideResultBox() {
    if (searchRsultShowFlag >= 1) {
        $("#searchResultListDropDown").hide();
    }
}
function showSearchFilter(data) {
    $("#searchResultListDropDown ul").html("");
    for (var i = 0; i < data.length; i++) {
        $("#searchResultListDropDown ul").append(
                "<li class='pr'>" +
                "<img src='/productimages/ThumbNail/" + data[i]["Image"] + "' />" +
                "<a href='/product/" + data[i]["PCode"] + "'>" + data[i]["PFName"] + "</a>" +
                "</li>" +
                "<li class='spr'>" +
                "<img class='img-responcive' src='/resources/images/sep.png' />" +
                "</li>"
            );
    }

}

//buy product button
$(document).on("click", ".buyButton", function () {
    addToBascket($(this).attr("data-pcode"), $("#buyNumber").val(), $("#bigImage").attr("data-tumbImage"), $(".bigPrice.price").attr("data-price"), $("#pName").html(), $("#combs li.selected").attr("data-combid"), $("#combs li.selected a").html());
    loadBascketFromCookie();
    showMessage("محصول با موفقیت ثبت شد");
});

function addToBascket(pCode, count, image, price, pName, combId, combName) {
    var productBascket = readBascket();
    var existInBascketFlag = false;
    if (productBascket!=null) {
for (var i = 0; i < productBascket.length; i++) {
        if (productBascket[i].pCode==pCode) {
            existInBascketFlag = true;
            productBascket[i].count =parseInt(productBascket[i].count)+parseInt($("#buyNumber").val());
        }
    }
    }
    else {
        productBascket = [];
    }
    if (!existInBascketFlag) {
        productBascket.push(
            {
                "pCode": pCode,
                "count": count,
                "image": image,
                "price": price,
                "pName": pName,
                "comId": combId,
                "combName":combName
            }
            );
    }
    $.cookie('bascket', JSON.stringify(productBascket), { expires: 7, path: '/' });
};
function removeFromBascket(pCode) {
    var productBascket = readBascket();
    if (productBascket!=null) {
        var removedResultBascket = productBascket
               .filter(function (el) {
                   return el.pCode !== pCode;
               }
);
        $.cookie('bascket', JSON.stringify(removedResultBascket), { expires: 7, path: '/' });
    }
   
};
function readBascket() {
    if ($.cookie("bascket")!=null) {
        var productBascket = JSON.parse($.cookie("bascket"));
        return productBascket;
    }
    return null;
   
};




//--------------------------------- bascket div -------------------------------
$(document).ready(function () {
    loadBascketFromCookie();
    $("#searchTextBox").width($("#searchBox").width() - 100);
    
});
$(window).resize(function () {
    $("#searchTextBox").width($("#searchBox").width() - 100);
});
function loadBascketFromCookie() {

    try {
        $("#bascketDiv ul").html("");
        var pCount = 0;
        var summaryPrice = 0;
        var bascketObj = readBascket();
        console.log(bascketObj);
        for (var i = 0; i < bascketObj.length; i++) {
            pCount++;
            summaryPrice += bascketObj[i]["price"] * bascketObj[i]["count"];
            $("#bascketDiv ul").append
                (
                "<li>" +
                "<img src='" + bascketObj[i]["image"] + "' />" +
                "<div>" +
                bascketObj[i]["pName"] +
                "</div>" +
                "</li>"
                );
            $("#productNumbers").html(pCount + " محصول");
            $("#sumPrice").html(summaryPrice+" تومان");
        }
    } catch (e) {

    }
}
$(document).on("click", "#bascketBtn", function () {
    loadBascketFromCookie();
    $("#bascketDiv").slideToggle();
});
$(document).ready(function () {
    $("#productView #combs ul li a").first().trigger("click");
})
$(document).on("click", "#productView #combs ul li a", function () {
    $("#productView #combs ul li a").parent().removeClass("selected");
    $(this).parent().addClass("selected");
    $(".bigPrice.price").html($(this).parent().attr("data-price"));
    $(".bigPrice.price").attr("data-price",$(this).parent().attr("data-price"));
});
