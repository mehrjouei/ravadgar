﻿function funjs_getFormObject(formid) {
    var prefix = $("#" + formid).attr("data-prefix");
    var obj = {};
    var fieldName = "";
    var fieldType = "";
    var fieldValue = "";
    var dataType = "";
    $("#" + formid).find("input, textarea").each(function (index) {
        fieldName = $(this).attr("name");
        fieldType = $(this).attr("type");
        fieldValue = $(this).val();
        if (fieldType == "checkbox") {
            if ($(this).is(':checked')) {
                obj[fieldName] += fieldValue;
            }
        }
        else {
            dataType = $(this).attr("data-type");
            if (dataType == "int") {
                obj[fieldName] = parseInt(fieldValue);

            }
            else if (dataType == "float") {
                //console.log("float");
                obj[fieldName] = parseFloat(fieldValue);
            }
            else if (dataType == "dateTime") {
                obj[fieldName] = fieldValue + ":00";
            }
            else {
                obj[fieldName] = fieldValue;
            }
        }

    });
    $("#" + formid).find("select").each(function (index) {
        fieldName = $(this).attr("name");
        //fieldName = fieldName.slice(prefix.length + 1).slice(0, -1);
        fieldType = $(this).attr("type");
        fieldValue = $(this).val();
        dataType = $(this).attr("data-type");
        if (dataType == "int") {
            obj[fieldName] = parseInt(fieldValue);
        }
        else {
            if (fieldType == "date") {
                obj[fieldName] = new Date(fieldValue);
            } else {
                obj[fieldName] = fieldValue;
            }
        }

    });
    return obj;
}


function funjs_sendObjectToServer(url, object) {
    var returnvalue = "";
    $.ajax({
        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },
        type: "POST",
        url: url,
        data: JSON.stringify(object),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function (xhr) {
        },
        success: function (data) {
            returnvalue= data;
        },
        error: function (data) {
            returnvalue = "ajaxError";
        },
        failure: function (errMsg) {
            returnvalue = "ajaxFailure";
        }
    });
    return returnvalue;
}

function jsonToHtmlTable(jsondata) {
    var returndata = "<table class='table table-bordered eTable'>";
    var obj = JSON.parse(jsondata);
    var count = Object.keys(obj).length;
    var keysbyindex = Object.keys(obj);
    for (var colIndex = 0 ; colIndex < count ; colIndex++) {
        var cellName = keysbyindex[colIndex];
        var cellValue = obj[keysbyindex[colIndex]];

        returndata += '<tr>' + "<td>" + cellName + "</td><td>" + cellValue + "</td></tr>";
    }
    returndata += "</table>";
    return returndata;
}

function showLoadingOn(place) {
    $(place).css("position", "relative");
    $(place).append("<div class='loadingDiv'><img src='/resources/images/userpanel/loading.gif' /></div>");
}
function hideLoadingOn(place) {
    $(place + " .loadingDiv").remove();
}

$(document).on("mouseenter", ".hover:not(.hover-hold)", function () {
    $(this).find('.desc').slideDown(500);
});
$(document).on("mouseleave", ".hover:not(.hover-hold)", function () {
    $(this).find('.desc').slideUp(500);
});


function calculatePrice(fp, cp) {
    return Math.round(fp * cp * 1.07).toFixed(0);
}
function calculatePriceFromRial(rp, cp) {
    console.log(Math.round((rp / cp) * 0.93).toFixed(2));
    return Math.round(parseFloat(rp) / parseFloat(cp) * 0.93 * 100) / 100;
}

function commaSeparated(val) {
    if (val == null || val == "") {
        return 0;
    }
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}
function deCommaSeprated(val) {
    return val.toString().replace(/\,/g, '');
}
$(document).ready(function () {
    $("#fPrice").keyup(function () {
        $(this).val(deCommaSeprated($(this).val()));
        $("#rialPrice").val(commaSeparated(calculatePrice($(this).val(),deCommaSeprated( $("#currencyPriceDiv").html()))));
        $(this).val(commaSeparated($(this).val()));
    });
    $("#fPrice").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#rialPrice").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#rialPrice").keyup(function () {
        if ($(this).val() == "") {
            $("#fPrice").val("0");
        }

        else {
            $(this).val(deCommaSeprated($(this).val()));
            $("#fPrice").val(calculatePriceFromRial($(this).val(),deCommaSeprated($("#currencyPriceDiv").html())));
            $(this).val(commaSeparated($(this).val()));
        }
    });
});
var opened = false;

$(document).on('mouseenter', "#userConsole.signedIn", function () {
    if (!opened) {
        opened = true;
        $(this).animate({
            left: '0px'
        }, "slow", function () {
        });
    }
});

$(document).on('mouseleave', "#userConsole.signedIn", function () {
    $(this).animate({
        left: '-110px'
    }, "slow", function () {
        opened = false;
    });

});

$(document).ready(function () {
    $(".chargeSubmitHome").click(function () {
        window.location.href = "/user/charge?cvalue="+deCommaSeprated($(".homeRialPrice").val());
    });
});
var errorArray = {
    "101": "ایمیل تکراری میباشد",
    "102": "تصویر امنیتی صحیح نمی باشد",
    "103": "اطلاعات ورود به پنل صحیح نمی باشد",
    "104": "لطفا ابتدا وارد پنل کاربری خود شوید",
    "105": "موجودی حساب کافی نیست لطفا حساب خود را شارژ کنید",
    "106": "پسور فعلی را صحیح وارد نمایید",
    "107": "ایمیل وارد شده معتبر نیست.",
    "108":" متاسفانه درخواست مورد نظر شما پیدا نشد. برای بررسی از حساب کاربری خود به مدیر سایت پیام ارسال نمایید.",
    "ajaxError": "خطا در اتصال به سرور",
    "Error":"خطا در سرور- دوباره تلاش کنید"
};
function showMessage(messageValue) {
    $("#messageBody").html(messageValue);
    $('#messageModal').modal({
        show: 'true'
    });
}