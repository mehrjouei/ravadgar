﻿var rangeMax = 2000000;
var rangeMin = 5000;
var filterStr = "";

$(document).ready(function () {
    $("#priceRange").ionRangeSlider({
        hide_min_max: true,
        keyboard: true,
        min: rangeMin,
        max: rangeMax,
        type: 'double',
        step: 5000,
        prefix: "تومان-",
        grid: true,
        grid_num: 3,
        onFinish: function (data) {
            var min = -1;
            var max = -1;
            if (data.from>rangeMin) {
                min=data.from;
            }
            if (data.to<rangeMax) {
                max=data.to;
            }
            changePriceRange(min, max);
        }
    });

    loadFilter();
    
    })
function loadFilter() {
    var url = window.location.href;
    if (url.split("#").length>1) {
        filterStr = url.split("#")[1];
        showProductList(doFilter(filterStr));
    }
}
window.onhashchange = loadFilter;
loadFilter();
function haveCondition(ProductFeatures,price, filterStr) {
    var name, value;
    filterStr = decodeURIComponent(filterStr.toLowerCase());
    var filters = filterStr.split("&");
    for (var i = 0; i < filters.length; i++) {
        if (filters[i].split(">").length > 1 && filters[i].split(">")[0] == "price") {
            name = decodeURIComponent((filters[i].split(">")[0]).toLowerCase());
            value = decodeURIComponent((filters[i].split(">")[1]).toLowerCase());
            if (parseInt(price) < parseInt(value)) {
                    return false;
                }
        }
        else if (filters[i].split("<").length > 1 && filters[i].split("<")[0] == "price") {
            name = decodeURIComponent((filters[i].split("<")[0]).toLowerCase());
            value = decodeURIComponent((filters[i].split("<")[1]).toLowerCase());
            if (parseInt(price) > parseInt(value)) {
                return false;
            }
        }
        else if (filters[i].split("=").length > 1) {
            name = decodeURIComponent((filters[i].split("=")[0]).toLowerCase());
            value = decodeURIComponent((filters[i].split("=")[1]).toLowerCase());
           
            for (var j = 0; j < ProductFeatures.length; j++) {
                if (ProductFeatures[j][0] == name && ProductFeatures[j][1] != value) {
                    return false;
                }
            }
        }
        else if (filters[i].split(">").length > 1) {
            name = decodeURIComponent((filters[i].split(">")[0]).toLowerCase());
            value = decodeURIComponent((filters[i].split(">")[1]).toLowerCase());

            for (var j = 0; j < ProductFeatures.length; j++) {
                if (ProductFeatures[j][0] == name && ProductFeatures[j][1] < value) {
                    return false;
                }
            }
        }
        else if (filters[i].split("<").length > 1) {
            name = decodeURIComponent((filters[i].split("<")[0]).toLowerCase());
            value = decodeURIComponent((filters[i].split("<")[1]).toLowerCase());

            for (var j = 0; j < ProductFeatures.length; j++) {
                if (ProductFeatures[j][0] == name && ProductFeatures[j][1] > value) {
                    return false;
                }
            }
        }
    }
    return true;
}
function doFilter(filterStr) {
    var list = [];
    $.each(obj, function (key, value) {
        if (haveCondition(value.ProductFeatures, value.Price,filterStr)) {
            list.push({
                'PCode': key,
                'PFName': value.PFName,
                'image': value.image,
                'price': value.Price
            });
        }
    });
    return list;
}
function showProductList(list) {
    var prStr = "";
    for (var i = 0; i < list.length; i++) {
        prStr += '<div class="col-md-3">'
               + '<div class="productBox">'
               + '<a href="/product/' + list[i].PCode + '">'
               + '<img alt="' + list[i].PFName + '" src="/productimages/ThumbNail/' + list[i].image + '" class="img-responsive">'
               + '<div class="bottomPart">'
               + '<p class="pTitr">' + list[i].PFName + '</p>'
               + '<p class="price">'+list[i].price+'</p>'
               + '</div>'
               + '</a>'
               + '</div>'
               + '</div>';
    }
    $("#productList").html(prStr);
}
function changePriceRange(min, max) {
    var url = decodeURIComponent(window.location.href.toLowerCase());
    if (url.split("#").length > 1 && url.split("#")[1].length>0) {
        var fstring = url.split("#")[1];
        var fParts = fstring.split("&");
        var minFlag = false;
        var maxFlag = false;
        fstring = "";
        for (var i = 0; i < fParts.length; i++) {
            if (fParts[i].split(">")[0] == "price" && min!=-1) {
                fParts[i] = "price>" + min;
                minFlag = true;
            }
            if (fParts[i].split("<")[0] == "price" && max!=-1) {
                fParts[i] = "price<" + max;
                maxFlag = true;
            }
            fstring += fParts[i];

            if (i < fParts.length - 1) {
                fstring += "&";
            }
        }
        if (!minFlag && min!=-1) {
            fstring += "&" + "price>" + min;
        }
        if (!maxFlag && max != -1) {
            fstring += "&" + "price<" + max;
        }
        window.location.href = url.split("#")[0] + "#" + fstring;

    }
    else {
        var flag = 0;
        var str = "";
        if (min!=-1) {
            str = "price>" + min;
            flag = 1;
        }
        if (max!=-1 && flag==1) {
            str += str = "&price<" + max
        }
        if (max != -1 && flag != 1) {
            str += str = "price<" + max
        }
        window.location.href=window.location.href + "#" + str;
    }
}