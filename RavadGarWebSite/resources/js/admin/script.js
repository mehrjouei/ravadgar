﻿function funjs_getFormObject(formid) {
    var prefix = $("#" + formid).attr("data-prefix");
    var obj = {};
    var fieldName = "";
    var fieldType = "";
    var fieldValue = "";
    var dataType = "";
    $("#" + formid).find("input, textarea").each(function (index) {
        fieldName = $(this).attr("name");
        fieldType = $(this).attr("type");
        fieldValue = $(this).val();
        if (fieldType == "checkbox") {
            if ($(this).is(':checked')) {
                obj[fieldName] += fieldValue;
            }
        }
        else {
            dataType = $(this).attr("data-type");
            if (dataType == "int") {
                obj[fieldName] = parseInt(fieldValue);

            }
            else if (dataType == "float") {
                //console.log("float");
                obj[fieldName] = parseFloat(fieldValue);
            }
            else if (dataType == "dateTime") {
                obj[fieldName] = fieldValue + ":00";
            }
            else {
                obj[fieldName] = fieldValue;
            }
        }

    });
    $("#" + formid).find("select").each(function (index) {
        fieldName = $(this).attr("name");
        //fieldName = fieldName.slice(prefix.length + 1).slice(0, -1);
        fieldType = $(this).attr("type");
        fieldValue = $(this).val();
        dataType = $(this).attr("data-type");
        if (dataType == "int") {
            obj[fieldName] = parseInt(fieldValue);
        }
        else {
            if (fieldType == "date") {
                obj[fieldName] = new Date(fieldValue);
            } else {
                obj[fieldName] = fieldValue;
            }
        }

    });
    return obj;
}


function funjs_sendObjectToServer(url, object) {
    $.ajax({
        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },
        type: "POST",
        url: url,
        data: JSON.stringify(object),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function (xhr) {
        },
        success: function (data) {
            return data;
        },
        error: function (data) {

        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function jsonToHtmlTable(jsondata) {
    var returndata = "<table class='table table-bordered eTable'>";
    var obj = JSON.parse(jsondata);
    var count = Object.keys(obj).length;
    var keysbyindex = Object.keys(obj);
    for (var colIndex = 0 ; colIndex < count ; colIndex++) {
        var cellName = keysbyindex[colIndex];
        var cellValue = obj[keysbyindex[colIndex]];

        returndata+= '<tr>'+"<td>"+cellName+"</td><td>"+cellValue+"</td></tr>";
    }
    returndata += "</table>";
    return returndata;
}



$(document).on("mouseenter", ".hover:not(.hover-hold)", function () {
    $(this).find('.desc').slideDown(500);
});
$(document).on("mouseleave", ".hover:not(.hover-hold)", function () {
    $(this).find('.desc').slideUp(500);
});


function calculatePrice(fp, cp) {
    return Math.round(fp * cp * 1.07).toFixed(0);
}
function commaSeparated(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}
function deCommaSeprated(val) {
    return val.toString().replace(",", "");
}
