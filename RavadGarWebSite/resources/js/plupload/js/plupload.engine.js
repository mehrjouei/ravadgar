//This function generate unique HASH identifier for each uploader
function generateUniqueId(uploaderId)
{
    var d = new Date();
    $("#" + uploaderId).parent().attr("id", "uploader" + String(d.getUTCMilliseconds()) + String(d.getUTCSeconds()) + String(d.getUTCMinutes()) + String(d.getUTCHours()) + Math.floor((Math.random() * 99999999) + 1));
}

//Convert jquery-ui progress bar to bootstrap progress bar for better viewing
function makeBootstrapProgressBar()
{
    element = $(".plupload_progress_container");
    element.removeClass();
    element.addClass("progress");

    element = $(".ui-progressbar-value");
    element.removeClass();
    element.addClass("progress-bar progress-bar-success progress-bar-striped active");
}


//Initialize all tooltips and popvers in the uploader page
function makeTooltipsAndPopovers()
{
    $('[data-toggle="tooltip"]').tooltip();

    $('.show-details').each(function() {

        var box = $(this).parents(".file-box").find(".details-box");
        var text = box.html();
        $(this).popover({
            content: text,
            html: true,
            trigger: "click|focus",
//            container: "body"
        });
    });
}


//If any error occured during file upload, animated error notification shows up and notifies the user
function showErrorNotification(message, uploaderId)
{
    errorResponses = window[uploaderId + "_errorResponses"];
    window[uploaderId + "_errorCount"];
    errorResponses.push(message);
    window[uploaderId + "_errorCount"]++;
    if ($("#" + uploaderId + " .plupload_header_content .uploadStatus").length)
    {
        $("#" + uploaderId + " .plupload_header_content .uploadStatus .error-count").html(window[uploaderId + "_errorCount"]);
    }
    else
    {
        $("#" + uploaderId + " .plupload_header_content").prepend(
                '<div class="uploadStatus"  style="float: left;" onclick="showErrors(' + "'" + uploaderId + "'" + ');" >' +
                '<span class="fa fa-times-circle"></span><span class="error-count">' + window[uploaderId + "_errorCount"] + '</span>' +
                '</div>'
                );
        $("#" + uploaderId + " .plupload_header_content .uploadStatus").tooltip({
            "title": "برای مشاهده خطاها کلیک کنید"
        });

        window[uploaderId + "_errorResponses"] = errorResponses;


    }

    jQuery.fn.animate = jQuery.fn.velocity;
    $("#" + uploaderId + " .plupload_header_content .uploadStatus").velocity({"left": "10"}, {
        duration: 100
    });
    $("#" + uploaderId + " .plupload_header_content .uploadStatus").velocity({"left": "0"}, {
        duration: 100
    });
    $("#" + uploaderId + " .plupload_header_content .uploadStatus").velocity({"left": "10"}, {
        duration: 100
    });
    $("#" + uploaderId + " .plupload_header_content .uploadStatus").velocity({"left": "0"}, {
        duration: 100
    });
}

//Shows error messages in the modal
function showErrors(uploaderId)
{
    //console.log(uploaderId);
    errorResponses = window[uploaderId + "_errorResponses"];
    errorCount = window[uploaderId + "_errorCount"];
    var errorContent = '<ul class = "list-group">';
    for (i = 0; i < errorResponses.length; i++)
    {
        errorContent += '<li class="list-group-item alert alert-danger">' + errorResponses[i] + '<span class="fa fa-times" style="float:left"></span></li>';
    }
    errorContent += '</ul>';
    $("#uploader-modal .modal-body").html(errorContent);
    $("#uploader-modal .modal-footer").remove();
    $("#" + uploaderId + " .plupload_header_content .uploadStatus").tooltip('hide');
    $("#uploader-modal").modal();
    $("#" + uploaderId + " .plupload_header_content .uploadStatus").fadeOut(function() {
        $(this).remove();
    });
    window[uploaderId + "_errorResponses"] = new Array();
    window[uploaderId + "_errorCount"] = 0;
}


//Shows up a confirm modal dialog
function showDeleteConfirm(id, uploaderId, url)
{
    $("#uploader-modal .modal-body").html(gridMessages['fileDeleteConfirmMessage']);
    $("#uploader-modal .modal-footer").remove();
    $("#uploader-modal .modal-content").append("<div class='modal-footer'></div>");
    $("#uploader-modal .modal-footer").html(
            '<button type="button" class="btn btn-default" data-dismiss="modal">' + 'انصراف' + '</button>' +
            '<button type="button" class="btn btn-danger" onclick="deleteFile(' + id + ',' + "'" + uploaderId + "'" + "," + "'" + url + "'" + ')">' + 'تایید' + '</button>');
    $("#uploader-modal").modal();
}

//This event raises after any file uploading
function FileUploadedEvent(up, file, object, uploaderId, addToPanel, deleteUrl)
{
    var response = $.parseJSON(object.response);
    $(".plupload_action_icon.glyphicon.glyphicon-trash").remove();
    if (response.status == "success")
    {
        if (!addToPanel) {
            $("ul#" + uploaderId + "_filelist li#" + file.id + " .plupload_file_fields").after("<input class='uploadedId' type='hidden' value=" + response.fileDatabaseId + " />");
            $("ul#" + uploaderId + "_filelist").find("li#" + file.id + " .plupload_file_action").append(
                    '<div class="remove-file glyphicon glyphicon-trash" onclick="removeFile($(this),' + "'" + uploaderId + "'" + "," + "'" + deleteUrl + "'" + ')"></div>'
                    );
        }

        return true;
    }
    else
    {
        $("ul#" + uploaderId + "_filelist").find("li#" + file.id).css({'background-image': 'url("../views/utility/resources/img/cancel.png"', 'background-color': '#F88695'});
        showErrorNotification(response.message, uploaderId);
        return false;
    }
    $("ul#" + uploaderId + "_filelist").find("li#" + file.id + " .plupload_file_action .plupload_action_icon").remove();
}

//This event raises after any error
function ErrorEvent(up, file, uploaderId)
{
    //This mean that the error is client side error. in fact this error have been raise before ajax request
    if (file.response == null)
    {
        showErrorNotification($("#" + uploaderId + " .plupload_message").find("i").text(), uploaderId);
        $("#" + uploaderId + " .plupload_message").remove();
    }

    //Otherwise the error is server side error
    else
    {
        var response = $.parseJSON(file.response);
        showErrorNotification(response.message, uploaderId);
        $("#" + uploaderId + " .plupload_message").remove();
    }
}


//This event raises after any add to or remove from download queue.
function QueueChangedEvent(uploaderId)
{
    setTimeout(function() {
        if ($("ul#" + uploaderId + "_filelist li").length >= 1)
            $("#" + uploaderId + "_dropbox" + " .plupload_droptext").css("display", "none");
        else
            $("#" + uploaderId + "_dropbox" + " .plupload_droptext").css("display", "block");
    }, 500);

}

//Send file database id to the server and delete the file
function ajaxDeleteFile(fileId, element, uploaderId, url)
{
    $.ajax({
        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },
        type: "POST",
        url: pageVars.contextPath + url,
        data: JSON.stringify(fileId),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function(xhr) {
            funjs_layoutLoadingShow();
        },
        success: function(data) {
            funjs_layoutLoadingHide();
            //console.log(data);
            if (element != null)
            {
                if (data.status == "success")
                {
                    var id = element.parent().parent().attr("id");
                    var file = $("#" + uploaderId).plupload("getFile", id);
                    $("#" + uploaderId).plupload("removeFile", file);
                }
                else
                    showErrorNotification(data.message, uploaderId);

            }
            else
            {
                if (data.status == "success")
                {
                    $("#file-box" + fileId).parent().fadeOut(function() {
                        $(this).remove();
                    });
                }
                else
                    showErrorNotification(data.message, uploaderId);
            }


        },
        error: function(data) {
            funjs_layoutLoadingHide();
            var exception = data.responseJSON;
            showErrorNotification(exception.message, uploaderId);

        },
        failure: function(errMsg) {

        }
    });
}


//Remove file from the uploader queue
function removeFile(element, uploaderId, url)
{
    var fileId = element.parent().parent().find(".uploadedId").attr("value");
    ajaxDeleteFile(Number(fileId), element, uploaderId, url);
}

function deleteFile(id, uploaderId, url)
{
    $("#uploader-modal").modal('hide');
    ajaxDeleteFile(Number(id), null, uploaderId, url);
}

function show()
{
    //console.log(window["uploader_asasnameh" + "_errorResponses"]);
    //console.log(window["uploader_asasnameh" + "_errorCount"]);
}

function addUploadedFileToUploadedPanel(fileObj, uploadedPanelId, uploaderId, deleteText, downloadText, detailsText, downloadUrl, deleteUrl, viewUrl, selectHeaderText, statusText, normalImageText)
{
    var htmlOutput ='<div class="col-md-3 col-sm-4 col-xs-12" style="display:none; margin-top: 10px;">';
    htmlOutput +=       '<div class="file-box file-box-waiting" id="file-box' + fileObj.fileDatabaseId + '">';
    htmlOutput +=           '<div class="actions-box">';
    htmlOutput +=               '<div style="float:right">';
    htmlOutput +=                   '<span data-toggle="tooltip" data-placement="bottom" title="' + deleteText + '" class="action-item glyphicon glyphicon-trash" style="color: red;" onclick="showDeleteConfirm(' + "'" + fileObj.fileDatabaseId + "'" + "," + "'" + uploaderId + "'" + "," + "'" + deleteUrl + "'" + ')"></span>';
    htmlOutput +=                   '<a tabindex="100" data-toggle="tooltip" data-placement="bottom" title="' + detailsText + '" class="action-item show-details fa fa-list" style="color: green;"></a>'
    htmlOutput +=                   '<a href="' + downloadUrl + '" target="_blank" data-toggle="tooltip" data-placement="bottom" title="' + downloadText + '" class="action-item fa fa-download" style="color: #0cb4b4;"></a>';
    htmlOutput +=                   '<span data-toggle="tooltip" data-placement="bottom" title="'+selectHeaderText+'" class="action-item fa fa-check selectHeader" style="color: green;" onclick="selectHeaderImage('+"'"+fileObj.fileDatabaseId+ "'"+');"></span>'
    htmlOutput +=               '</div>';
    htmlOutput +=               '<div style="float:left">';
    htmlOutput +=                   '<span data-toggle="tooltip" class="fa fa-clock-o" style="color: orange; font-size: 22px" title="'+statusText+'"></span>';
    htmlOutput +=               '</div>';
    htmlOutput +=               '<div style="clear:both"></div>';
    htmlOutput +=           '</div>';
    htmlOutput +=           '<div class="thumbnail-box"><img src="'+ viewUrl +'" /></div>';
    htmlOutput +=               '<div class="file-name" data-toggle="tooltip" data-placement="bottom" title="' + fileObj.fileName + '" >';
    htmlOutput +=                   normalImageText;
    htmlOutput +=               '</div>';
    htmlOutput +=           '<div class="details-box" id="file_details' + fileObj.fileDatabaseId + '" data-content="">';
    htmlOutput +=               '<table class="table table-bordered">';
    htmlOutput +=                    '<tbody>';
    htmlOutput +=                        '<tr>';
    htmlOutput +=                           '<td><b>نام فایل: </b></td>';
    htmlOutput +=                           '<td>' + fileObj.fileName + '</td>';
    htmlOutput +=                        '</tr>';
    htmlOutput +=                       '<tr>';
    htmlOutput +=                           '<td><b>نوع فایل: </b></td>';
    htmlOutput +=                           '<td>' + fileObj.extension + '</td>';
    htmlOutput +=                       '</tr>';
    htmlOutput +=                       '<tr>';
    htmlOutput +=                           '<td><b>زمان ایجاد: </b></td>';
    htmlOutput +=                           '<td>' + fileObj.createDateInfo + '</td>';
    htmlOutput +=                       '</tr>';
    htmlOutput +=                   '</tbody>';
    htmlOutput +=               '</table>';
    htmlOutput +=           '</div>';
    htmlOutput +=       '</div>';
    htmlOutput += '</div>';
    $("#" + uploadedPanelId).append(htmlOutput);
    $("#file-box" + fileObj.fileDatabaseId).parent().fadeIn();



}

